import Dashboard from '../layouts/Dashboard/Dashboard.jsx';
import Login from '../views/Login/Login.jsx';
import Register from '../views/Register/Register.jsx';
import Facturas from '../views/Facturas/Facturas.jsx';

var indexRoutes = [
	{ path: "/register", name: "Register", component: Register },
    { path: "/login", name: "Login", component: Login },
    { path: "/", name: "Home", component: Dashboard },

];

export default indexRoutes;
