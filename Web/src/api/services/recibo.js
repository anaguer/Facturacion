import Base from './base';

let server = '';
if (process.env.NODE_ENV === 'development') {
	server = 'http://localhost:8000/api';
} else {
	server = '/api'
}

class Recibo extends Base {

	All = (token, page, search, value) => {
		return this.get('recibo',{token, page, search, value});
	}

	Create = (token , data) => {
		return this.post('recibo',{token, ...data});
	}
	
	Get = ( token, id ) => {
		return this.get('recibo/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('recibo', {token, id});
	}

	Update = (token, data) => {
		return this.put('recibo', {...data, token});
	}
}

export default new Recibo(server)
