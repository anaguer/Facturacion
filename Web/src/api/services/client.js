import Base from './base';

let server = '';
if (process.env.NODE_ENV === 'development') {
	server = 'http://localhost:8000/api';
} else {
	server = '/api'
}

class Client extends Base {

	All = (token,page) => {
		return this.get('client',{token,page});
	}

	Create = (token , data) => {
		return this.post('client',{token, ...data});
	}
	Get = ( token, id ) => {
		return this.get('client/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('client', {token, id});
	}

	Update = (token, data) => {
		return this.put('client', {...data, token});
	}
}

export default new Client(server)
