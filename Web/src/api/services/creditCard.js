import Base from './base';

let server = '';
if (process.env.NODE_ENV === 'development') {
	server = 'http://localhost:8000/api';
} else {
	server = '/api'
}

class CreditCard extends Base {

	All = (token,page, search, value) => {
		return this.get('creditcard',{token, page, search, value});
	}

	Create = (token , data) => {
		return this.post('creditcard',{token, ...data});
	}
	Get = ( token, id ) => {
		return this.get('creditcard/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('creditcard', {token, id});
	}

	Update = (token, data) => {
		return this.put('creditcard', {...data, token});
	}
}

export default new CreditCard(server)