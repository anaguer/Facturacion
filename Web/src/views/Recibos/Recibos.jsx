import React from 'react';

import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';


import {
    Button,  Row, Col, Modal,  ModalHeader,  ModalBody,  ModalFooter,  Input,  InputGroup, FormGroup, Table, Card,
    CardHeader,
    CardBody,
    CardTitle,
    FormFeedback,
    Label,
    Pagination,
    PaginationItem,
    PaginationLink,
    FormText
} from 'reactstrap';


import {
    PanelHeader,
    Checkbox,
    Radio
} from '../../components';

import language from "../../api/translator/translator"
import Cookies from 'react-cookies';
import swal from 'sweetalert';
import Loader from 'react-loader-spinner';
import Recibo from '../../api/services/recibo';
import Cliente from '../../api/services/client';
import Factura from '../../api/services/factura';

class Recibos extends React.Component{

  constructor(props) {
    super(props);

    this.state = {visible: false,
                  toggle: false,
                  recibos:[],
                  facturas:[],
                  allFacturas:[],
                  factura:{},
                  clientes:[],
                  total:[],
                  client_id:-1,
                  User:Cookies.load('userId'),
                  administrador:'0',
                  updateBirdcall:false,
                  nombre:'',
                  err:false,
                  seeRecibos:false,
                  page:1,
                  search:undefined,
                  value:undefined,
                  searchFactura:undefined,
                  pageFactura:1,
                  valueFactura:undefined,
                  totalFactura:[],
                 };

    this.toggleModal = this.toggleModal.bind(this);
    this.createBirdcall = this.createBirdcall.bind(this);
    this.updateBirdcall = this.updateBirdcall.bind(this);
    this.seeFacturas = this.seeFacturas.bind(this);
    this.getData(this.state.page);
    this.backPage = this.backPage.bind(this);
    this.addPage = this.addPage.bind(this);
    Cliente.All(this.state.User.access_token)
      .then(data=>this.setState({clientes: data ? data : []}));
    this.getDataFactura(this.state.pageFactura)

  }

  searchData(search, value){
    if(this.state.search === search && this.state.value===value){
      this.setState({page:1, search:undefined, value:undefined})
      this.getData(1);
      return;
    }
    this.setState({page:1, search, value})
    this.getData(1,search, value );
  }
  getData(page, search, value){
    Recibo.All(this.state.User.access_token, page, search, value)
      .then(data=>{
          const total = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            total.push(i)
          }
          console.log(data)
          this.setState({recibos: data  ? data.data : [], total})
        });

  }
  addPage() {
    if (this.state.page === this.state.total.length) {
      return;
    }
    this.getData(this.state.page+1, this.state.search, this.state.value);
    this.setState({page:this.state.page+1});

  }

  backPage() {
    if (this.state.page === 1) {
      return;
    }
    this.getData(this.state.page-1, this.state.search, this.state.value);
    this.setState({page:this.state.page-1});

  }

  goToPage(index) {
    this.getData(index, this.state.search, this.state.value);
    this.setState({page:index});

  }

  searchDataFactura(search, value){
    if(this.state.searchFactura === search && this.state.valueFactura===value){
      this.setState({pageFactura:1, searchFactura:undefined, valueFactura:undefined})
      this.getDataFactura(1);
      return;
    }
    this.setState({pageFactura:1, searchFactura:search, valueFactura:value})
    this.getDataFactura(1,search, value );
  }

  getDataFactura(page, search, value){
    Factura.All(this.state.User.access_token, page, search, value)
      .then(data=>{
          const total = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            total.push(i)
          }
          console.log(data)
          this.setState({allFacturas: data  ? data.data : [], totalFactura:total})
        });

  }

  addPageFactura() {
    if (this.state.pageFactura === this.state.totalFactura.length) {
      return;
    }
    this.getDataFactura(this.state.pageFactura+1, this.state.searchFactura, this.state.valueFactura);
    this.setState({pageFactura:this.state.pageFactura+1});

  }

  backPageFactura() {
    if (this.state.pageFactura === 1) {
      return;
    }
    this.getDataFactura(this.state.pageFactura-1, this.state.searchFactura, this.state.valueFactura);
    this.setState({pageFactura:this.state.pageFactura-1});

  }

  goToPageFactura(index) {
    this.getDataFactura(index, this.state.searchFactura, this.state.valueFactura);
    this.setState({pageFactura:index});

  }
  showChart() {
    return (
      <PanelHeader
          size="sm"
      />
    )
  }

  toggleModal() {
    this.setState({
      createBirdcallModal: !this.state.createBirdcallModal,
      fecha:'',
      descripcion:'',
      facturas_id:0,
      client_id:0,
      id:null,
      forma:'',
      updateBirdcall:false,
      loading:false
    });
  }

  createBirdcall(){
    let bandera = true;
    let errores = {errores:{errNombre:'', errDni:'', errEmail:'', errEmail2:'', errCiudad:'', errAp:'', errTelefono:'',
                    errZona:'', errCodigoPostal:'', errBarrio:'', errFechaNac:'', errDomicilio: ''}};

    console.log(this.state)
    if (this.state.fecha === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.facturas_id === 0) {
      bandera = false;
      this.setState({err:true})
    }


    if (this.state.descripcion === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.forma === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (bandera) {
      this.setState({loading:true, disabled:true});
      Recibo.Create(this.state.User.access_token,this.state)
        .then((data)=>{
          console.log(data);
          this.state.recibos.push(data);
          this.setState({createBirdcallModal:false})
          swal(language("Recibo") + ` ${this.state.descripcion} ` + language("MsjCreadoExito"),{icon:"success", button:false, timer:2000})
        })
    }
    this.setState({...this.state, errores:errores});

  }

  deleteListener(index) {
    swal({
      title: language("MsjEliminarRecibo") + ` ${this.state.recibos[index].nombre} ?`,
      text: language("MsjNoRecuperar"),
      type:'warning',
      icon:'warning',
      showCancelButton:true,
      button: {
        text:language("BotonEliminar"),
        className:'btn-danger',
        onClick:(()=>{console.log("click")})
      },
      closeOnConfirm:false
    }).then(willDelete=>{
      if(!willDelete) return;
      Recibo.Delete(this.state.User.access_token, this.state.users[index].id)
        .then((data)=>{
          swal(language("MsjEliminado"), language("MsjUsuarioEliminado") +` ${this.state.users[index].nombre}`+language("MsjUsuarioEliminado2"), 'success')
          this.setState({users:this.state.users.filter((data,i)=>i!==index)})
        })
        .catch(error=>{
          swal(language("Error"), language("MsjErrorEliminandoRecibo") + ` ${this.state.users[index].nombre}`, 'danger')
        })

    })


  }

  openUpdate(index){
    this.setState({...this.state.recibos[index], createBirdcallModal:true, updateBirdcall:true, indexToEdit:index})
  }

  updateBirdcall(){
     Recibo.Update(this.state.User.access_token, this.state)
      .then((data)=>{
        this.state.users[this.state.indexToEdit] = data.user;
        this.setState({createBirdcallModal:false, updateBirdcall:false})
        swal(language("Recibo") + `${this.state.nombre}` + language("Editado"),{icon:"success", button:false, timer:2000})
      })
      this.setState({updateBirdcall:false})
  }

  verRecibos(){
    this.setState({seeRecibos:true});
  }

  cerrarRecibos(){
    this.setState({seeRecibos:false});
  }

  seeFacturas(event) {
    Factura.FacturaForClient(this.state.User.access_token, event.target.value)
      .then(data=>this.setState({facturas: data ? data : []}));
    this.state.facturas_personales_id = event.target.value;
    console.log("cliente", this.state.facturas_personales_id)
  }

  saveFactura(event) {
    console.log(event.target.value)
    Factura.Get(this.state.User.access_token, event.target.value)
      .then(data=>this.setState({factura: data ? data : {}}));
    this.state.facturas_id = event.target.value;
    //console.log("factura_id",this.state.facturas_id)

  }

  render(){
    console.log(this.state.recibos)
        return (
            <div>
               {this.showChart()}

            {!this.state.seeRecibos ?
            <div style={{'paddingLeft':'2%', 'paddingRight':'2%', 'paddingTop':'2%', 'backgroundColor':'white'}}>
            <Row style={{'marginLeft':'2%'}}>
              <FormGroup>
                <div>
                  {language("CreateName")}
                </div>
                <InputGroup>
                  <Input style={{width:'200px'}} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>this.searchDataFactura( event.target.value === '-1' ? undefined: 'personales_id', event.target.value === '-1' ? undefined: event.target.value)} required={true}>
                    <option value="-1">{language("OpcionSelecciona")}</option>
                    {
                      this.state.clientes.map((clientes, i)=>{
                        return(
                          <option value={clientes.id}>{clientes.nombre}</option>
                        );
                      })
                    }
                  </Input>
                </InputGroup>
              </FormGroup>
               <FormGroup style={{'marginLeft':'1%'}}>
                <div>
                  {language("MetodoPago")}
                </div>
                <InputGroup>
                  <Input style={{width:'200px'}} type="select" id="metodo_pago" name="MetodoPago" onChange={(event)=>this.searchDataFactura( event.target.value === '-1' ? undefined: 'tipo', event.target.value === '-1' ? undefined: event.target.value)} required={true}>
                    <option value="-1">{language("OpcionSelecciona")}</option>
                    <option value={1}>{language("Efectivo")}</option>
                    <option value={2}>{language("TarjetaCredito")}</option>
                    <option value={3}>{language("PorCuotas")}</option>
                  </Input>
                </InputGroup>
              </FormGroup>
               <FormGroup style={{'marginLeft':'1%'}}>
                  <div>
                    {language("Cantidad")}
                  </div>
                  <InputGroup>
                    <Input style={{width:'100px'}} type="number" id="codigo" name="codigo" placeholder="1212" onChange={(event)=>this.setState({cantidad:event.target.value})} required={true}/>
                  </InputGroup>
                </FormGroup>
            </Row>

            <Card>
              <CardBody>
                <div className="show-scroll">
                  <Table hover responsive>
                    <thead className="text-primary">
                      <tr>
                        <th className="justify-content-center text-center">{"Nº"}</th>
                        <th className="justify-content-center text-center">{language("DetalleArticulo")}</th>
                        <th className="justify-content-center text-center">{language("Vencimiento")}</th>
                        <th className="justify-content-center text-center">{language("Mora")}</th>
                        <th className="justify-content-center text-center">{language("Intereses")}</th>
                        <th className="justify-content-center text-center">{language("Importe")}</th>
                        <th className="justify-content-center text-center">{"Total"}</th>
                        <th className="justify-content-center text-center">{language("FechaPago")}</th>
                        <th className="justify-content-center text-center">{language("Abonado")}</th>
                        <th className="justify-content-center text-center">{language("Moneda")}</th>
                        <th className="justify-content-center text-center">{language("Saldo")}</th>
                      </tr>
                    </thead>
                    <tbody>
                       {
                        this.state.allFacturas.map((Facturas, i)=>{
                          return (
                            <tr style={{backgroundColor:this.state.value === Facturas.id ? '#eee' :'transparent' }} onClick={()=>this.searchData('codfactura', Facturas.id)} key = {i}>
                              <td className="justify-content-center text-center">
                                {Facturas.num_factura}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.descripcion}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.vencimiento}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.mora}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.intereses}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.haber}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.importe_pagado}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.fecha_pago}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.debe}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.tipo}
                              </td>
                              <td className="justify-content-center text-center">
                                {Facturas.saldo}
                              </td>
                              </tr>
                            );
                          })
                        }
                    </tbody>
                  </Table>
                </div>
                <Pagination aria-label="Page navigation example">
                    <PaginationItem>
                      <PaginationLink style={{backgroundColor:this.state.pageFactura === 1 ? '#eee' : 'white'}} previous onClick={this.backPageFactura} />
                    </PaginationItem>
                    {this.state.totalFactura.map((data, i)=>{
                      if ((this.state.totalFactura.length > 15 && i < this.state.pageFactura +15 && i >= this.state.pageFactura )|| this.state.totalFactura.length <= 15) {
                              return(
                                  <PaginationItem>
                                    <PaginationLink style={{backgroundColor:this.state.pageFactura === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPageFactura(i+1)}>
                                      {i+1}
                                    </PaginationLink>
                                  </PaginationItem>
                                );
                        }

                    })}
                    <PaginationItem>
                      <PaginationLink style={{backgroundColor:this.state.pageFactura === this.state.totalFactura.length ? '#eee' : 'white'}} next onClick={this.addPageFactura} />
                    </PaginationItem>
                  </Pagination>
              </CardBody>
            </Card>

            <div style={{backgroundColor:'#ddd', width:'100%', height:'3px', marginTop:'2%'}}>
            </div>
            <Row style={{marginTop:'2%', 'marginLeft':'2%'}} >
                <FormGroup>
                  <div>
                    {language("FechaPago")}
                  </div>
                  <InputGroup>
                    <Input style={{width:'200px'}} type="date" id="fecha" name="nombre" placeholder="" onChange={(event)=>this.setState({nombre:event.target.value})} required={true}/>
                  </InputGroup>
                </FormGroup>
                <div style={{'marginLeft':'4%', 'marginTop': '1.5rem'}} >
                  <Checkbox
                    label={language("ModificarFactura")}
                    inputProps={{defaultChecked: true}}
                  />
                </div>
                <div style={{marginTop:'2%', 'marginLeft':'4%','fontWeight':'bold'}}>
                  {language("FacturaCancelar")}
                </div>
                 <FormGroup style={{'marginLeft':'4%'}}>
                  <div>
                    {language("DeudaTotal")}
                  </div>
                  <InputGroup>
                    <Input value={"120,00"} style={{width:'200px'}} type="text" id="metodo_pago" name="MetodoPago" onChange={(event)=>this.setState({tipo:event.target.value})} required={true}/>
                  </InputGroup>
                </FormGroup>
                <Button style={{'marginLeft':'4%', 'height':'40px'}}>
                  {"Admin"}
                </Button>
            </Row>

            <Card>
              <CardBody>
                <div className="show-scroll">
                  <Table hover responsive>
                    <thead className="text-primary">
                      <tr>
                        <th className="justify-content-center text-center">{language("Factura")}</th>
                        <th className="justify-content-center text-center">{language("DetalleArticulo")}</th>
                        <th className="justify-content-center text-center">{language("Intereses")}</th>
                        <th className="justify-content-center text-center">{language("Importe")}</th>
                        <th className="justify-content-center text-center">{language("Recibo")}</th>
                        <th className="justify-content-center text-center">{language("Mora")}</th>
                        <th className="justify-content-center text-center">{language("Opciones")}</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        this.state.recibos.map((Recibos, i)=>{
                          return (
                            <tr key = {i}>
                              <td className="justify-content-center text-center">
                                {Recibos.codfactura}
                              </td>
                              <td className="justify-content-center text-center">
                                {Recibos.descripcion}
                              </td>
                              <td className="justify-content-center text-center">
                                {Recibos.intereses}
                              </td>
                              <td className="justify-content-center text-center">
                                {Recibos.importe}
                              </td>
                              <td className="justify-content-center text-center">
                                {Recibos.id}
                              </td>
                              <td className="justify-content-center text-center">
                                {"20"}
                              </td>
                              <td className="justify-content-center ">
                                <tr>
                                  <a target='_black' href={"/pdf/generarrecibo?accion=get&id="+ Recibos.id}><i className="now-ui-icons files_single-copy-04" >{language("VerRecibo")}</i></a>
                                </tr>
                              </td>
                            </tr>
                            );
                          })
                        }
                    </tbody>
                    <Pagination aria-label="Page navigation example">
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === 1 ? '#eee' : 'white'}} previous onClick={this.backPage} />
                      </PaginationItem>
                      {this.state.total.map((data, i)=>{
                        if ((this.state.total.length > 15 && i < this.state.page +15 && i >= this.state.page )|| this.state.total.length <= 15) {
                              return(
                                <PaginationItem>
                                  <PaginationLink style={{backgroundColor:this.state.page === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPage(i+1)}>
                                    {i+1}
                                  </PaginationLink>
                                </PaginationItem>
                              );
                          }
                      })}
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === this.state.total.length ? '#eee' : 'white'}} next onClick={this.addPage} />
                      </PaginationItem>
                    </Pagination>
                  </Table>
                </div>
              </CardBody>
            </Card>

            <Row style={{marginTop:'2%'}}>
              <Button style={{'marginLeft':'4%', 'height':'40px'}}>
                  {language("Actualizar")}
              </Button>
              <div style={{'marginLeft':'4%', marginTop:'2%'}}>
                {language("MensajeRecibos")}
              </div>
              <Input style={{marginTop:'1%', 'marginLeft':'4%', width:'65px', 'height':'40px'}} type="text" id="metodo_pago" name="MetodoPago" onChange={(event)=>this.setState({tipo:event.target.value})} required={true}/>
              <div style={{marginTop:'2%', 'marginLeft':'16%','fontWeight':'bold'}}>
                {language("ImportePagar")}
              </div>
              <div style={{marginTop:'2%', 'marginLeft':'4%','fontWeight':'bold'}}>
                {"110,00"}
              </div>
              <div style={{marginTop:'2%', 'marginLeft':'4%','fontWeight':'bold'}}>
                {language("Saldo")}
              </div>
              <div style={{marginTop:'2%', 'marginLeft':'4%','fontWeight':'bold'}}>
                {"10,00"}
              </div>
            </Row>

            <Row style={{marginTop:'2%'}}>
              <Input style={{ width:'450px', 'height':'40px', 'marginLeft':'2%'}} type="text" id="metodo_pago" name="MetodoPago" onChange={(event)=>this.setState({tipo:event.target.value})} required={true}/>
              <Button style={{'marginLeft':'4%', 'height':'40px'}}>
                  {language("BotonGuardar")}
              </Button>
              <Button style={{'marginLeft':'4%', 'height':'40px'}}>
                  {language("Imprimir")}
              </Button>
              <Button color="primaryBlue" style={{'marginLeft':'28%', 'height':'40px'}} onClick={this.toggleModal}>
                  {language("BotonCrear")}
              </Button>
              <Button color="primaryBlue" style={{'marginLeft':'2%', 'height':'40px'}} onClick={this.verRecibos.bind(this)}>
                  {language("VerRecibos")}
              </Button>
            </Row>



            <Modal className="boton-volver" style={{}} isOpen={this.state.createBirdcallModal} toggle={()=>{ this.setState({createBirdcallModal:false})}} >
                <ModalHeader  >
                    <table>
                      <tr>
                        <td>{ this.state.updateBirdcall ? language('BotonEditar') : language("BotonRegistrar")}</td>
                      </tr>
                    </table>
                  </ModalHeader>

                  { !this.state.loading ?

                    <div xs={10} md={2} lg={2} style={{paddingTop:'2%', width:'80%', marginLeft:'10%'}}>
                        <FormGroup>
                          <div>
                            {language("Fecha")}
                          </div>
                          <InputGroup>
                            <Input value={this.state.fecha} onChange={(event)=>this.setState({fecha:event.target.value})} type="date" id="fecha" name="Fecha" required={true}/>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("Clientes")}
                          </div>
                          <InputGroup>

                            <Input style={{width:'200px'}} type="select" id="fecha" name="Fecha" placeholder="" onChange={(event)=>{this.seeFacturas(event)}} required={true}>
                              <option value="-1">{language("OpcionSelecciona")}</option>
                              {
                                this.state.clientes.map((clientes, i)=>{
                                  return(
                                    <option value={clientes.dni}>{clientes.nombre}</option>
                                  );
                                })
                              }
                            </Input>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("Facturas")}
                          </div>
                          <InputGroup>

                            <Input style={{width:'200px'}} type="select" id="factura_id" name="Factura" onChange={(event)=>{this.saveFactura(event)}} required={true}>
                              <option value="-1">{language("OpcionSelecciona")}</option>
                              {
                                this.state.facturas.map((factura, i)=>{
                                  return(
                                    <option value={factura.id}>{factura.num_factura + " " + factura.descripcion}</option>
                                  );
                                })
                              }
                            </Input>
                          </InputGroup>
                        </FormGroup>
                         <FormGroup style={{'marginLeft':'1%'}}>
                          <div>
                            {language("MetodoPago")}
                          </div>
                          <InputGroup>
                            <Input style={{width:'200px'}} type="select" id="metodo_pago" name="MetodoPago" onChange={(event)=>this.setState({forma:event.target.value})} required={true}>
                              <option value={language("Efectivo")}>{language("Efectivo")}</option>
                              <option value={language("TarjetaCredito")}>{language("TarjetaCredito")}</option>
                            </Input>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("DetalleArticulo")}
                          </div>
                          <InputGroup>
                            <Input value={this.state.descripcion} onChange={(event)=>this.setState({descripcion:event.target.value})} type="text" id="register_name" name="Name" placeholder={language("DetalleArticulo")} required={true}/>
                          </InputGroup>
                        </FormGroup>
                        <Row style={{marginLeft:'0px'}}>
                          <FormGroup>
                            <div>
                              {language("Importe") + " Original"}
                            </div>
                            <InputGroup>
                              <Input style={{width:'130px'}} value={this.state.factura.importe_pagado} type="text" id="importe_original" name="ImporteOriginal" disabled required={true}/>
                            </InputGroup>
                          </FormGroup>
                          <FormGroup style={{marginLeft:'2%'}}>
                            <div>
                              {language("Importe")}
                            </div>
                            <InputGroup>
                              <Input style={{width:'130px'}} onChange={(event)=>this.setState({importe:event.target.value})} type="number" id="importe" name="Password" placeholder="12" required={true}/>
                            </InputGroup>
                          </FormGroup>
                          <FormGroup style={{marginLeft:'2%'}}>
                            <div>
                              {language("Intereses")}
                            </div>
                            <InputGroup>
                              <Input disable style={{width:'130px'}} value={this.state.facturas.bon_intereses} type="number" id="intereses" name="intereses" placeholder="12" required={true}/>
                            </InputGroup>
                          </FormGroup>
                        </Row>
                        { this.state.err ?
                           <div className="errores">{language("ErrorCampo")}</div>
                           : null
                          }
                      </div>

                    :

                      <div className="flex-center" style={{height:'350px'}}>
                        <Loader
                           type="TailSpin"
                           color="#2ca8FF"
                           height="50"
                           width="50"
                        />
                      </div>
                    }
                  <ModalFooter >
                    <Button disabled={this.state.disabled} className="boton-volver" style={{marginRight: '38%'}} onClick={this.state.updateBirdcall ? this.updateBirdcall.bind(this) : this.createBirdcall.bind(this)}>{ this.state.updateBirdcall ? language('BotonEditar') : language("BotonRegistrar")}</Button>
                  </ModalFooter>
                </Modal>

            </div>
            :
            <div style={{'paddingLeft':'2%', 'paddingRight':'2%', 'paddingTop':'2%', 'backgroundColor':'white'}}>
              <Button color="primaryBlue" style={{'height':'40px'}} onClick={this.cerrarRecibos.bind(this)}>
                  {language("BotonRegresar")}
              </Button>
              <Card>
              <CardBody>
                <Table hover responsive>
                  <thead className="text-primary">
                    <tr>
                      <th className="justify-content-center text-center">{language("Factura")}</th>
                      <th className="justify-content-center text-center">{language("DetalleArticulo")}</th>
                      <th className="justify-content-center text-center">{language("Intereses")}</th>
                      <th className="justify-content-center text-center">{language("Importe")}</th>
                      <th className="justify-content-center text-center">{language("Recibo")}</th>
                      <th className="justify-content-center text-center">{language("Mora")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {

                      this.state.recibos.map((Recibos, i)=>{
                        console.log(Recibos)
                        return (
                          <tr key = {i}>
                            <td className="justify-content-center text-center">
                              {Recibos.codfactura}
                            </td>
                            <td className="justify-content-center text-center">
                              {Recibos.descripcion}
                            </td>
                            <td className="justify-content-center text-center">
                              {Recibos.intereses}
                            </td>
                            <td className="justify-content-center text-center">
                              {Recibos.importe}
                            </td>
                            <td className="justify-content-center text-center">
                              {Recibos.id}
                            </td>
                            <td className="justify-content-center text-center">
                              {"20"}
                            </td>
                          </tr>
                          );
                        })
                      }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
            </div>
          }
          </div>
        );
    }
}

export default Recibos;
