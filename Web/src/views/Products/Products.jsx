import React from 'react';

import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';

import ReactPaginate from 'react-paginate';
import {
    Button,  Row, Col, Modal,  ModalHeader,  ModalBody,  ModalFooter,  Input,  InputGroup, FormGroup, Table, Card,
    CardHeader,
    CardBody,
    Pagination,
    PaginationItem,
    PaginationLink
} from 'reactstrap';


import {
    PanelHeader
} from '../../components';

import language from "../../api/translator/translator"
import Cookies from 'react-cookies';
import swal from 'sweetalert';
import Loader from 'react-loader-spinner';

import Product from '../../api/services/product';
class Productos extends React.Component{

  constructor(props) {
    super(props);

    this.state = {visible: false,
                  toggle: false,
                  Products:[],
                  User:Cookies.load('userId'),
                  administrador:'0',
                  permiso:0,
                  err:false,
                  page:1,
                  loading:false,
                  disabled:false,
                  total:[]
                 };

    this.toggleModal = this.toggleModal.bind(this);
    this.updateProduct = this.updateProduct.bind(this);
    this.backPage = this.backPage.bind(this);
    this.addPage = this.addPage.bind(this);
    this.getData(this.state.page);
  }
  

  getData(page){
    Product.All(this.state.User.access_token, page)
      .then(data=>{
          const total = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            total.push(i)  
          }
          console.log(data)
          this.setState({Products: data  ? data.data : [], total})
        });
  
  }
  showChart() {
    return (
      <PanelHeader
          size="sm"
      />
    )
  }

  addPage() {
    if (this.state.page === this.state.total.length) {
      return;
    }
    this.getData(this.state.page+1);
    this.setState({page:this.state.page+1});
    
  }

  backPage() {
    if (this.state.page === 1) {
      return;
    }
    this.getData(this.state.page-1);
    this.setState({page:this.state.page-1});
    
  }
 
  goToPage(index) {
    this.getData(index);
    this.setState({page:index});
    
  }

  toggleModal() {
    this.setState({
      createUserModal: !this.state.createUserModal,
      codigo_art:'',
      detalle_art:'',
      unitario:'',
      id:null,
      updateProduct:false,
      procentaje:'',
      loading:false,
      disabled:false,
    });
  }

  createProduct(){

    let bandera = true;
    let errores = {errores:{errNombre:'', errDni:'', errEmail:'', errEmail2:'', errCiudad:'', errAp:'', errTelefono:'',
                    errZona:'', errCodigoPostal:'', errBarrio:'', errFechaNac:'', errDomicilio: ''}};

    
    if (this.state.codigo_art === "" ) {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.detalle_art === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.unitario === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.porcentaje === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (bandera) {
      this.setState({loading:true, disabled:true});
      Product.Create(this.state.User.access_token,this.state)
        .then((data)=>{
          if (data.error) {
            swal('Oops...',data.error,'warning')
            this.setState({loading: false, disabled: false})
          } else {
            this.setState({loading: true, disabled: true})
            console.log(data);
            this.state.Products.push(data);
            this.setState({createUserModal:false, loading: false, disabled: false})
            swal(language("Producto") + ` ${this.state.codigo_art} ` + language("MsjCreadoExito"),{icon:"success", button:false, timer:2000})
          }
        })
    }
  }

  deleteListener(index) {
    swal({
      title: language("MsjEliminarProducto") + ` ${this.state.Products[index].codigo_art} ?`,
      text: language("MsjNoRecuperar") ,
      type:'warning',
      icon:'warning',
      buttons:
      {
        exit:{
          text:language("BotonCancelar")
        },
        delete:{
          text:language("BotonEliminar"),
          className:'btn-danger'
        },


      }
    }).then(willDelete=>{
      if(willDelete!== 'delete') return;
      Product.Delete(this.state.User.access_token, this.state.Products[index].id_ar)
        .then((data)=>{
          swal(language("MsjEliminado"), language("MsjProductoEliminado") +` ${this.state.Products[index].codigo_art}`+language("MsjUsuarioEliminado2"), 'success')
          this.setState({Products:this.state.Products.filter((data,i)=>i!==index)})
        })
        .catch(error=>{
          swal(language("Error"), language("ErrorEliminando") + ` ${this.state.Products[index].codigo_art} ${language('detailErrorProducto')}`, 'warning')
        })

    })


  }


  openUpdate(index){
    this.setState({...this.state.Products[index], createUserModal:true, updateProduct:true, indexToEdit:index})
  }

  updateProduct(){
    this.setState({loading:true, disabled:true});
     Product.Update(this.state.User.access_token, this.state)
      .then((data)=>{
        if (data.error) {
          swal('Oops...',data.error,'warning')
          this.setState({loading: false, disabled: false})
        }else {
          this.setState({loading: true, disabled: true})
          this.state.Products[this.state.indexToEdit] = data;
          this.setState({createUserModal:false, updateProduct:false, loading: false, disabled: false})
          swal(language("Producto") + ` ${this.state.detalle_art} ` + language("Editado"),{icon:"success", button:false, timer:2000})
        }

      })
  }
  
  render(){
    console.log(this.state.Products)
        return (
            <div>
            {this.showChart()}
              { this.state.Products[0] != null ?

                <Card>

                  <CardBody>

                    <Table hover responsive>
                      <thead className="text-primary">
                        <tr>
                          <th className="justify-content-center text-center">{language("Codigo")}</th>
                          <th className="justify-content-center text-center">{language("DetalleArticulo")}</th>
                          <th className="justify-content-center text-center">{language("Precio")}</th>
                          <th className="justify-content-center text-center">{language("CreatePorcentaje")}</th>
                          <th className="justify-content-center text-center">{language("Opciones")}</th>
                        </tr>
                      </thead>
                      <tbody>
                        { this.state.Products.map((Products, i)=>{
                          return(
                            <tr key = {i}>
                              <td className="justify-content-center text-center">
                                {Products.codigo_art}
                              </td>
                              <td className="justify-content-center text-center">
                                {Products.detalle_art}
                              </td>
                              <td className="justify-content-center text-center">
                                {Products.unitario}
                              </td>
                              <td className="justify-content-center text-center">
                                {Products.porcentaje}
                              </td>

                              <td className="justify-content-center ">
                                <tr>
                                  <i className="now-ui-icons files_single-copy-04" onClick={()=>this.openUpdate(i)}>{language("BotonEditar")}</i>
                                </tr>
                                <tr><i className="now-ui-icons shopping_credit-card" onClick={()=>this.deleteListener(i)}>{language("BotonEliminar")}</i></tr>
                              </td>
                            </tr>
                          );

                        })}
                      </tbody>
                    </Table>
                    <Pagination aria-label="Page navigation example">
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === 1 ? '#eee' : 'white'}} previous onClick={this.backPage} />
                      </PaginationItem>
                      {this.state.total.map((data, i)=>{
                        if ((this.state.total.length > 15 && i < this.state.page +15 && i >= this.state.page )|| this.state.total.length <= 15) {
                            return(
                              <PaginationItem>
                                <PaginationLink style={{backgroundColor:this.state.page === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPage(i+1)}>
                                  {i+1}
                                </PaginationLink>
                              </PaginationItem>
                            );
                        }
                      })}
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === this.state.total.length ? '#eee' : 'white'}} next onClick={this.addPage} />
                      </PaginationItem>
                    </Pagination>
                  </CardBody>
                </Card>

              :

              <div className="content flex-center animated animatedFadeInUp fadeInUp">
                  <h2>{language("MsjCrearProductos")}</h2>
              </div>

              }
              <div style={{paddingBottom:'1%', paddingRight:'1%'}} className="fixed-right justify-content-right text-right">
                <Button color="primaryBlue" style={{width:'50px', height:'50px'}} className="btn-round btn-icon" onClick={this.toggleModal}>
                  <i className="now-ui-icons ui-1_simple-add"></i>
                </Button>
              </div>
              <Modal className="boton-volver" style={{}} size='md' isOpen={this.state.createUserModal} toggle={()=>{ this.setState({createUserModal:false})}} >
                <ModalHeader  >
                    <table>
                      <tr>
                        <td>{ this.state.updateProduct ? language('BotonEditar') : language("BotonRegistrar")}</td>
                      </tr>
                    </table>
                  </ModalHeader>

                  { !this.state.loading ?

                    <div xs={10} md={2} lg={2} style={{paddingTop:'2%', width:'80%', marginLeft:'10%'}}>

                      <FormGroup>
                        <div>
                          {language("Codigo")}
                        </div>
                        <InputGroup>
                          <Input value={this.state.codigo_art} onChange={(event)=>this.setState({codigo_art:event.target.value, err:false})} type="text" id="register_codigo" name="Codigo" placeholder="AA-1212" required={true}/>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <div>
                          {language("DetalleArticulo")}
                        </div>
                        <InputGroup>
                          <Input value={this.state.detalle_art} onChange={(event)=>this.setState({detalle_art:event.target.value, err:false})} type="text" id="detalle_art" name="detalle_art" placeholder={language("MsjBreveDescripcion")} required={true}/>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <div>
                          {language("Precio")}
                        </div>
                        <InputGroup>
                          <Input value={this.state.unitario} onChange={(event)=>this.setState({unitario:event.target.value, err:false})} type="number" id="precio" name="precio" placeholder="100" required={true}/>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <div>
                          {language("CreatePorcentaje")}
                        </div>
                        <InputGroup>
                          <Input value={this.state.porcentaje} onChange={(event)=>this.setState({porcentaje:event.target.value, err:false})} type="number" id="porcentaje" name="porcentaje" placeholder="12" required={true}/>
                        </InputGroup>
                      </FormGroup>
                      { this.state.err ?
                       <div className="errores">{language("ErrorCampo")}</div>
                       : null
                      }
                    </div>

                  :

                    <div className="flex-center" style={{height:'288px'}}>
                      <Loader
                         type="TailSpin"
                         color="#2ca8FF"
                         height="50"
                         width="50"
                      />
                    </div>
                  }
                  <ModalFooter >
                    <Button disabled={this.state.disabled} className="boton-volver" style={{marginRight: '38%'}} onClick={this.state.updateProduct ? this.updateProduct : this.createProduct.bind(this)}>{ this.state.updateProduct ? language('BotonEditar') : language("BotonRegistrar")}</Button>
                  </ModalFooter>
                </Modal>
            </div>
             
        );
    }
}

export default Productos;
