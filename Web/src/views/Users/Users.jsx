import React from 'react';
import {
    Button,  Row, Modal,  ModalHeader,  ModalBody,  ModalFooter,  Input,  InputGroup, FormGroup, Table, Card,
    CardBody,
    Pagination,
    PaginationItem,
    PaginationLink
} from 'reactstrap';


import {
    PanelHeader,
    Checkbox,
} from '../../components';

import language from "../../api/translator/translator"
import Cookies from 'react-cookies';
import swal from 'sweetalert';
import Loader from 'react-loader-spinner';
import User from '../../api/services/user';

class users extends React.Component{

  constructor(props) {
    super(props);

    this.state = {visible: false,
                  toggle: false,
                  users:[],
                  User:Cookies.load('userId'),
                  permiso:0,
                  err:false,
                  page:1,
                  loading:false,
                  disabled:false,
                  total:[],
                  administrador:0,

                 };

    this.toggleModal = this.toggleModal.bind(this);
    this.getData(this.state.page);
    this.backPage = this.backPage.bind(this);
    this.addPage = this.addPage.bind(this);


  }

  getData(page){
    User.getUsers(this.state.User.access_token, page)
      .then(data=>{
          const total = new Array;
          for (let i = 0; i < (data.total/5); i++) {
            total.push(i)
          }
          console.log(data)
          this.setState({users: data  ? data.data : [], total})
        });

  }

  addPage() {
    if (this.state.page === this.state.total.length) {
      return;
    }
    this.getData(this.state.page+1);
    this.setState({page:this.state.page+1});

  }

  backPage() {
    if (this.state.page === 1) {
      return;
    }
    this.getData(this.state.page-1);
    this.setState({page:this.state.page-1});

  }

  goToPage(index) {
    this.getData(index);
    this.setState({page:index});

  }


  showChart() {
    return (
      <PanelHeader
          size="sm"
      />
    )
  }

  toggleModal() {
    this.setState({
      createUserModal: !this.state.createUserModal,
      nombre:'',
      usuario:'',
      password:'',
      confirmPassword:'',
      id:null,
      updateUser:false,
      roles_idroles:1,
      loading:false,
    });
  }

  createUser(){
    let bandera = true;
    let errores = {errores:{errNombre:'', errDni:'', errCiudad:'', errAp:'', errTelefono:'',
                    errZona:'', errCodigoPostal:'', errBarrio:'', errFechaNac:'', errDomicilio: ''}};

    console.log(this.state)

    if (this.state.nombre === "" ) {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.usuario === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (this.state.password === "") {
      bandera = false;
      this.setState({err:true})
    }

    if (bandera) {
      this.setState({loading:true, disabled:true});
      User.register(this.state)
        .then((data)=>{
          if (data.error) {
            swal('Oops...',data.error,'warning')
            this.setState({loading: false, disabled: false})
          } else {
            this.state.users.push(data.user);
            this.setState({createUserModal:false,loading:false, disabled:false})
            swal(language("Usuario") + ` ${this.state.nombre} `+ language("MsjCreadoExito"),{icon:"success", button:false, timer:2000, loading: false, disabled: false})
          }
        })
    }
  }

  deleteListener(index) {
    swal({
      title: language("MsjEliminarUsuario") + ` ${this.state.users[index].nombre} ?`,
      text: language("MsjNoRecuperar") ,
      type:'warning',
      icon:'warning',
      buttons:
      {
        exit:{
          text:"cancel"
        },
        delete:{
          text:language("BotonEliminar"),
          className:'btn-danger'
        },


      }
    }).then(willDelete=>{
      if(willDelete!== 'delete') return;
      User.deleteUsers(this.state.User.access_token, this.state.users[index].id)
        .then((data)=>{
          swal(language("MsjEliminado"), language("MsjUsuarioEliminado") +` ${this.state.users[index].nombre}`+language("MsjUsuarioEliminado2"), 'success')
          this.setState({users:this.state.users.filter((data,i)=>i!==index)})
        })
        .catch(error=>{
          swal(language("Error"), language("ErrorEliminando") + ` ${this.state.users[index].nombre}`, 'warning')
        })

    })


  }

  confirmPassword(event){
    if (event.target.value !== this.state.password) {
      this.setState( {passwordMatch: true} )
    } else {
      this.setState( {passwordMatch: false} )
    }
  }


  openUpdate(index){
    this.setState({...this.state.users[index], createUserModal:true, updateUser:true, indexToEdit:index})
  }

  updateUser(){
     this.setState({loading:true, disabled:true});
     User.updateUser(this.state.User.access_token, this.state)
      .then((data)=>{
        if (data.error) {
          swal('Oops...',data.error,'warning')
          this.setState({loading: false, disabled: false})
        } else {
          this.state.users[this.state.indexToEdit] = data.user;
          this.setState({createUserModal:false, updateUser:false, loading:false, disabled:false})
          swal(language("Usuario") +` ${this.state.nombre} ` + language("Editado"),{icon:"success", button:false, timer:2000, loading: false, disabled: false})
        }

      })
  }

  render(){
        return (
            <div>

              {this.showChart()}

              { this.state.users[0] != null ?

                <Card>

                  <CardBody>

                    <Table hover responsive>
                      <thead className="text-primary">
                        <tr>
                          <th className="justify-content-center text-center">{language("CreateName")}</th>
                          <th className="justify-content-center text-center">{language("Usuario")}</th>
                          <th className="justify-content-center text-center">{language("BotonEditar")}</th>
                          <th className="justify-content-center text-center">{language("BotonEliminar")}</th>
                        </tr>
                      </thead>
                      <tbody>
                        { this.state.users.map((users, i)=>{
                          return(
                            <tr key = {i}>
                              <td className="justify-content-center text-center">
                                {users.nombre}
                              </td>
                              <td className="justify-content-center text-center">
                                {users.usuario}
                              </td>
                              <td className="justify-content-center text-center">
                                <Button style={{width:'150px'}} color="primaryBlue" for="notification_file" onClick={()=>this.openUpdate(i)}>
                                  <i className='now-ui-icons ui-2_settings-90'></i>
                                </Button>
                              </td>
                              <td className="justify-content-center text-center">
                                <Button style={{width:'150px'}} color="primaryBlue" for="notification_file" onClick={()=>this.deleteListener(i)}>
                                  <i className='now-ui-icons ui-1_simple-remove'></i>
                                </Button>
                              </td>
                            </tr>
                          );

                        })}
                      </tbody>
                    </Table>
                    <Pagination aria-label="Page navigation example">
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === 1 ? '#eee' : 'white'}} previous onClick={this.backPage} />
                      </PaginationItem>
                      {this.state.total.map((data, i)=>{
                        return(
                          <PaginationItem>
                            <PaginationLink style={{backgroundColor:this.state.page === i+1 ? 'rgb(44, 168, 255,0.7)' : 'white'}} onClick={()=>this.goToPage(i+1)}>
                              {i+1}
                            </PaginationLink>
                          </PaginationItem>
                        );
                      })}
                      <PaginationItem>
                        <PaginationLink style={{backgroundColor:this.state.page === this.state.total.length ? '#eee' : 'white'}} next onClick={this.addPage} />
                      </PaginationItem>
                    </Pagination>
                  </CardBody>
                </Card>

              :

              <div className="content flex-center animated animatedFadeInUp fadeInUp">
                  <h2>{language("CreaUsuarios")}</h2>
              </div>

              }

              <div style={{paddingBottom:'1%', paddingRight:'1%'}} className="fixed-right justify-content-right text-right">
                <Button color="primaryBlue" style={{width:'50px', height:'50px'}} className="btn-round btn-icon" onClick={this.toggleModal}>
                  <i className="now-ui-icons ui-1_simple-add"></i>
                </Button>
              </div>

              <Modal className="boton-volver" style={{}} isOpen={this.state.createUserModal} toggle={()=>{ this.setState({createUserModal:false})}} >
                <ModalHeader  >
                    <table>
                      <tr>
                        <td>{ this.state.updateUser ? language('BotonEditar') : language("BotonRegistrar")}</td>
                      </tr>
                    </table>
                  </ModalHeader>

                  { !this.state.loading ?

                    <div xs={10} md={2} lg={2} style={{paddingTop:'2%', width:'80%', marginLeft:'10%'}}>
                        <FormGroup>
                          <div>
                            {language("CreateName") + " *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.nombre} onChange={(event)=>this.setState({nombre:event.target.value, err:false})} type="text" id="register_name" name="Name" placeholder={language("Tu") + " " + language("CreateName")} required={true}/>
                          </InputGroup>
                        </FormGroup>
                         <FormGroup>
                          <div>
                            {language("Usuario") + " *"}
                          </div>
                          <InputGroup>
                            <Input value={this.state.usuario} onChange={(event)=>this.setState({usuario:event.target.value, err:false})} type="text" id="register_name" name="Name" placeholder={language("Tu") + " " +language("Usuario")} required={true}/>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("CreatePassword") + " *"}
                          </div>
                          <InputGroup>
                            <Input onChange={(event)=>this.setState({password:event.target.value, err:false})} type="password" id="register_password" name="Password" placeholder={language("ErrorPasword")} required={true}/>
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <div>
                            {language("ConfirmarPassword") + " *"}
                          </div>
                          <InputGroup>
                            <Input onChange={(event)=>this.confirmPassword(event)} type="password" id="register_confirm_password" name="Password" placeholder={language("ConfirmarPassword")} required={true}/>
                          </InputGroup>
                        </FormGroup>
                        { this.state.passwordMatch === true ?
                          <p>{language("ErrorPasswordNoMatch")}</p>
                        : null }

                        { this.state.err ?
                         <div className="errores">{language("ErrorCampo")}</div>
                         : null
                        }
                        <div style={{'marginLeft':'1%'}} >
                          <Checkbox
                            label={language("OpcionAdministrador")}
                            inputProps={{defaultChecked: Boolean(this.state.administrador)}}
                            onChange={event=>this.setState({administrador:Number(!this.state.administrador)})}
                          />
                        </div>
                      </div>

                    :

                      <div className="flex-center" style={{height:'350px'}}>
                        <Loader
                           type="TailSpin"
                           color="#2ca8FF"
                           height="50"
                           width="50"
                        />
                      </div>
                    }
                  <ModalFooter >
                    <Button disabled={this.state.disabled} className="boton-volver" style={{marginRight: '38%'}} onClick={this.state.updateUser ? this.updateUser.bind(this) : this.createUser.bind(this)}>{ this.state.updateUser ? language('BotonEditar') : language("BotonRegistrar")}</Button>
                  </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default users;
