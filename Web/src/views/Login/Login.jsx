import React from 'react';
import {
    Card, CardHeader, CardBody, CardFooter, CardTitle, Button, Row, Col,  Input,  InputGroup, FormGroup
} from 'reactstrap';
// react plugin used to create charts
import swal from 'sweetalert';
// function that returns a color based on an interval of numbers
import Cookies from 'react-cookies';
import {
    PanelHeader
} from '../../components';


import ApiUser from '../../api/services/user'
import { Link } from 'react-router-dom';



class Login extends React.Component{


  constructor(props) {
    super(props);

    this.state = {usuario:"",
                  clave:"",
                  errores:{errEmail:"", errPassword:""}};

    }

  loginUser() {



    let bandera = true;
    let errores = {errores:{errEmail:'', errPassword:''}}

    if (this.state.usuario === "") {
      errores.errores.errEmail = "Debe introducir su usuario";
      bandera = false;
    }

    if (this.state.clave === "") {
      errores.errores.errPassword = "Introduzca su contraseña";
      bandera = false;
    }

      if (bandera) {
        ApiUser.login(this.state.usuario, this.state.clave)
          .then((data)=>{

            if(data){
              window.location.href="/Dashboard"
              Cookies.save('userId', data, { path: '/' })
              console.log(data)
            }

          }).catch(erro=>swal("credenciales incorrectas",{icon:"warning", button:false, timer:1500}))

      }
       this.setState({...this.state,errores:errores.errores})

  }

    render(){

        return (
            <div>
                <PanelHeader
                    size="sm"
                />
              <div className="content">
                    <Row className="justify-content-center">
                        <Col xs={12} md={4} className="justify-content-center">
                            <Card className="card-chart" style={{height: '300px'}}>
                                <CardHeader style={{paddingTop:'7%'}}>
                                    <CardTitle>Inicio de Sesion</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <div xs={10} md={2} lg={2} style={{paddingTop:'2%'}}>
                                      <FormGroup>
                                        <div>
                                          Usuario
                                        </div>
                                        <InputGroup>
                                          <Input onChange={(event)=>this.setState({usuario:event.target.value, errores:{...this.state.errores, errEmail:''}})} type="usuario" id="login_email" name="usuario" required={true}/>
                                        </InputGroup>
                                        <div className="errores">
                                          {this.state.errores.errEmail}
                                        </div>
                                      </FormGroup>
                                      <FormGroup>
                                        <div>
                                          Contraseña
                                        </div>
                                        <InputGroup>
                                          <Input onChange={(event)=>this.setState({clave:event.target.value, errores:{...this.state.errores, errPassword:''}})} type="password" id="login_password" name="clave" required={true}/>
                                        </InputGroup>
                                        <div className="errores">
                                          {this.state.errores.errPassword}
                                        </div>
                                      </FormGroup>
                                      <div >

                                      </div>
                                      <Link to={"/login"}>{"¿Olvido su contraseña?"}</Link>
                                    </div>
                                </CardBody>
                                <CardFooter className='justify-content-right text-right' style={{marginTop:'-2%'}}>
                                  <Button onClick={this.loginUser.bind(this)} color="primaryBlue" style={{width:'100px'}}>Iniciar Sesión</Button>
                                </CardFooter>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default Login;
