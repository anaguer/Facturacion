import { AppRegistry } from 'react-native';
import App from './app/index';

AppRegistry.registerComponent('Facturacion', () => App);

export default App;