import React from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import Proposal from '../services/proposal';
import Event from '../services/events';
import Firebase from '../config/firebase';
export default class HistorialScreen extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      proposal:[]
    };
    
    if(this.props.screenProps.user.user.roles_id === 1) {
       Proposal.getHistory(this.props.screenProps.user.token)
        .then((data)=>{
          console.log(data)
          this.setState({proposal:data ? data : []})
        })
    } else {
      Event.getEventsHistory(this.props.screenProps.user.token)
        .then((data)=>{
          console.log(data)
          if(!data)return;
          const aux = [];
          for (var i = 0; i < data.length; i++) {
            if(!data[i].proposals) continue;
             
            for (var j = data[i].proposals.length - 1; j >= 0; j--) {
              if(data[i].proposals[j].status > 1) {
                aux.push({events:data[i], proposal:data[i].proposals[j]})
                break;
              }
            }
          }
          this.setState({proposal:aux });
        })
    }/*
    Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}`)
      .on('value', (dataSnapshot) => { 
            const data = dataSnapshot.val();
            console.log(data)
            this.setState({proposal:data})
      })*/
   
  }
  /*
     Object.keys(this.state.proposal).map((key)=>(
          this.state.proposal[key].proposal.status > 1 ?
          <View style={styles.evento}>
            <Text style={styles.eventoTitulo}>{this.state.proposal[key].event.name}</Text>
            <View style={styles.eventoInfo}>
              <Text style={styles.eventoPrecio}>{"$" +  this.state.proposal[key].proposal.budget}</Text>
              <Text style={styles.eventoFecha}>{this.state.proposal[key].event.date.split(' ')[0]}</Text>
            </View>
            <Text style={styles.eventoUsuario}>{this.state.proposal[key].proposal.users.profile.name}</Text>
          </View> 
          :null
  */
  render() {
     console.log(this.state.proposal)
    return (
     
      <ScrollView style={styles.contenedor}>
        {
        
          this.props.screenProps.user.user.roles_id === 1 ? 
          this.state.proposal.map(data=>{
            if(data.status < 2) return null;
            return (
              <View style={styles.evento}>
                <Text style={styles.eventoTitulo}>{data.events.name}</Text>
                <View style={styles.eventoInfo}>
                  <Text style={styles.eventoPrecio}>{"$" +  data.budget}</Text>
                  <Text style={styles.eventoFecha}>{data.events.date.split(' ')[0]}</Text>
                </View>
                <Text style={styles.eventoUsuario}>{data.users.profile.name}</Text>
              </View>
            );
          })
          :this.state.proposal.map(data=>{
            return (
              <View style={styles.evento}>
                <Text style={styles.eventoTitulo}>{data.events.name}</Text>
                <View style={styles.eventoInfo}>
                  <Text style={styles.eventoPrecio}>{"$" +  data.proposal.budget}</Text>
                  <Text style={styles.eventoFecha}>{data.events.date.split(' ')[0]}</Text>
                </View>
                <Text style={styles.eventoUsuario}>{data.proposal.users.profile.name}</Text>
              </View>
            );
          })
        
      }
      </ScrollView>
  
    );
  }
}

const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    
  },

  evento: {
    padding: 20,
    borderBottomColor: "#ecf0f1",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    
  },

  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    fontWeight: 'normal',
    color: "#DEB749"
  },

  eventoInfo : {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  eventoPrecio : {
    fontWeight: 'bold',
  },
  eventoFecha : {

  },
  eventoUsuario : {
    marginTop: 10,
  }



});


