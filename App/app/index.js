import React, { Component } from 'react';
import { Root, AuthStack, ClientsRoot } from './config/router';
import LocalStorage from './config/LocalStorage';
import Firebase from './config/firebase';

class App extends Component {
	constructor(props) {
	  super(props);

	  this.state = {
	  	user:null,
	  };

	  this.logIn = this.logIn.bind(this);
	  this.logOut = this.logOut.bind(this);
	}
	componentWillMount(){
		this.calculateUser();
		Firebase.database().ref(`closeSession`)
        .on('value', (dataSnapshot) => {
            const data = dataSnapshot.val();
            if(data){
              if(data.value){
                this.setState({user:null})
                Firebase.database().ref(`closeSession`).set(null);
              }
            }

        });
	}
	async calculateUser() {
		this.state.user = await LocalStorage.getUser();
		this.setState({user:await LocalStorage.getUser();})
	}
	



	logIn(user) {
		LocalStorage.setUser(user);
		this.setState({ user });
	}

	logOut() {
		this.setState({ user:null });
	}

	render() {

		if(!this.state.loading)
			return <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
						<Image
				          style={{width:'100%', height:'100%'}}
				          source={require('./img/splash.png')}
				        /> 
			       </View>

		if(!this.state.user)
			return <AuthStack screenProps={{logIn:this.logIn}}/>

		if (this.state.user.role === "Usuario")
			return <Root screenProps={{user:this.state.user,openDrawer:this.openDrawer, logOut:this.logOut}}/>

		else if (this.state.user.role === "Cliente")
			return <ClientsRoot screenProps={{user:this.state.user,openDrawer:this.openDrawer, logOut:this.logOut}}/>
	}
}

export default App;
