import React from 'react';
import { Text, View, StyleSheet, ScrollView,TouchableOpacity, RefreshControl, ListView } from 'react-native';
import {Container,Header,Left, Right, Body, Content,  List, Icon, Button, Fab, Title } from 'native-base';
import Client from '../../services/client';
import Firebase from '../../config/firebase';
export default class clientScreen extends React.Component {


  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    
    this.state = {
      basic: true,
      clients:[]
    };
    this.getData = this.getData.bind(this);
    
    this.goToClientDetail = this.goToClientDetail.bind(this);
  }
  componentDidMount() {
      this.ref = Firebase.database().ref(`update/${this.props.screenProps.user.user.id}`)
        .on('value', (dataSnapshot) => { 
            const data = dataSnapshot.val();
            if(data){
              if(data.updateClients){
                this.getData();
                Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateClients`).set(null);
              }  
            }
            
        });
      this.getData();
  }
  
  

  getData(){
    this.setState({refreshing:true})
    Client.All(this.props.screenProps.user.token)
      .then(data=>{
        this.setState({refreshing:false ,clients: data ? data.reverse() : []})
      });
    
    
  }

  goToClientDetail(client) {
    
      this.props.navigation.navigate('ClientDetail', {client})
    
    
  }

  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.clients];
    const dataDelete = newData.splice(rowId, 1)[0]
    Client.Delete(this.props.screenProps.user.token,dataDelete.id)
      .then(data=>console.log(data))
    this.setState({ clients: newData });
  }

  render() {
    
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={this.props.navigation.openDrawer} transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Clientes</Title>
          </Body>
          <Right>
           
          </Right>
        </Header>
        <Content>
          <List
            leftOpenValue={75}
            rightOpenValue={-75}
            dataSource={this.ds.cloneWithRows(this.state.clients)}
            renderRow={(data,i) =>
              <TouchableOpacity key={i} style={[styles.evento, {borderBottomWidth:1, borderColor:'#DEB749', borderStyle:'solid'}]} onPress={()=>this.goToClientDetail(data)}>                
                <Text style={styles.eventoTitulo}>{`Nombre: ${data.nombre}`}</Text>
                <Text style={styles.eventoDescripcion}>{`Dni: ${data.dni}`}</Text>
                <Text style={styles.eventoFecha}>{`Email: ${data.email}`}</Text>
              </TouchableOpacity>
            }
            renderLeftHiddenRow={data =>
              <Button full warning onPress={()=>this.props.navigation.navigate('ClientCreate', {client:data})}>
                <Icon active name="md-create" />
              </Button>}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="trash" />
              </Button>}
          />
        </Content>
        <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.props.navigation.navigate('ClientCreate')}>
            <Icon name="md-add" />
            
          </Fab>
      </Container>
    );
  }
}
/**
 * <ScrollView
        refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.getData}
        />
        }
      >
        <View style={styles.contenedor}>
        {
          this.state.users.map((data,i)=>{
            return (
              <TouchableOpacity key={i} style={data.send ? [styles.evento, {borderWidth:2, borderColor:'#DEB749', borderStyle:'solid'}] : styles.evento} onPress={()=>this.goToClientDetail(data)}>                
                <Text style={styles.eventoTitulo}>{data.usuario}</Text>
                <Text style={styles.eventoDescripcion}>{data.email}</Text>
                <Text style={styles.eventoFecha}>{data.usuario}</Text>
              </TouchableOpacity>
            );
          })
        }
        </View>  
      </ScrollView>
 */
const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    padding: 20,
  },
  evento: {
    backgroundColor: "#fff",
    padding: 10,
    paddingRight:15,
    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    // borderBottomColor: "gray",
    // borderBottomWidth: 2,
  },

  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    color: "#DEB749"
  },

  eventoDescripcion : {
    color: "#212121"
  },

  eventoFecha : {
    marginTop: 10,
    color: "#212121"
  },



});


