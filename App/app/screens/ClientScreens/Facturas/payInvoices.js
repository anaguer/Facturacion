import React from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Text, Linking, ListView, Modal, WebView, ActivityIndicator} from 'react-native';
import { Container, Header, Right, Left, List, Title, Body, Content, Button, Icon, Thumbnail } from 'native-base';
import { Table, Row, Rows } from 'react-native-table-component';
import ERROR from '../../../config/Error';
import Firebase from '../../../config/firebase';
import Select from 'react-native-picker-select';
import Factura from '../../../services/factura'

export default class payInvoices extends React.Component {

    constructor(props) {
        super(props);
        let today = new Date();
        let dd = today.getDate() < 10 ? '0'+today.getDate() : today.getDate();
        let mm = today.getMonth()+1 < 10 ? '0'+(today.getMonth()+1) : (today.getMonth()+1);
        let yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;

        this.state = {
          starCount: 0,
          total:0,
          proposal:[],
          tipodoc:this.props.screenProps.user.user.tipodoc,
          dni:this.props.screenProps.user.user.dni,
          tipofactura:this.props.screenProps.user.user.tipofactura,
          condicioniva:this.props.screenProps.user.user.condicioniva,
          codtipodoc:this.props.screenProps.user.user.codtipodoc,
          invoices:this.props.navigation.state.params.selected,
          unselected:this.props.navigation.state.params.invoices,
          head:['#', 'Precio', 'Cantidad', 'Total', ''],
          isAcepted:false,
          proposalAcepted:{},
          isModal:false,
          isModalPay:false,
          vencimiento:today,
          selectedValue:[],
          flag:false,
          mercadoPagoModal:false
        };


        for (var i = 0; i < this.state.invoices.length; i++) {
          let debe = parseFloat(this.state.invoices[i].debe);
          this.state.total += debe;

        }

        this.payInvoices = this.payInvoices.bind(this);
        this.payMercadoPago = this.payMercadoPago.bind(this);
        this.generateCupo = this.generateCupo.bind(this);

        Factura.GetDocumentTypes()
          .then(tipos=>{this.setState({observaciones: tipos[tipos.map(data=>data.Desc).indexOf(this.state.tipofactura)].Id})})
    }

    payInvoices() {
      
    }

    convertFromHex(hex) {
      var hex = hex.toString();//force conversion
      var str = '';
      for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
      return str;
    }
    
    convertToHex(str) {
      var hex = '';
      for(var i=0;i<str.length;i++) {
        hex += ''+str.charCodeAt(i).toString(16);
      }
      return hex.substr(0,250);
    }

    handleSelect() {
      let total = 0;
      let id = this.state.selectedValue['id'];

      this.state.unselected = this.state.unselected.filter((data,i)=>(data['id']!==id));
      this.state.invoices.push(this.state.selectedValue);

      for (var i = 0; i < this.state.invoices.length; i++) {
        let debe = parseFloat(this.state.invoices[i].debe);
        total += debe;
      }

      this.setState({total:total, flag:false})
    }

    handleDelete(invoice,i) {
      let total = 0;

      this.state.invoices.splice(i,1);
      this.state.unselected.push(invoice);

      for (var i = 0; i < this.state.invoices.length; i++) {
        let debe = parseFloat(this.state.invoices[i].debe);
        total += debe;
      }

      this.setState({total:total})
    }
    
    payMercadoPago() {
      if (!this.state.invoices.length) {
        ERROR("No hay facturas selecionadas!!", "Error");
        return;
      }

      const dni =  this.props.screenProps.user.user.dni;
      const productsSelect = this.state.invoices;
      this.state.code = this.convertToHex(JSON.stringify(this.state))
      //console.log('code', this.state.code)
      this.setState({loadingMercadoPago:true});
      Firebase.database().ref(`updateuser`).on('value', snapshot=>{
          
          if (snapshot.val()) {
            if (snapshot.val()[this.state.code]) {
              if(snapshot.val()[this.state.code].payment.value){
                this.setState({mercadoPagoModal:false, loadingWeb:false })
                ERROR("Pago registrado", "Error");
                this.props.navigation.navigate('Invoices');
                Firebase.database().ref(`update/${this.props.screenProps.user.user['id']}`).set({updateFacturas:true})

              } else {
                this.setState({mercadoPagoModal:false, loadingWeb:false})
                ERROR("Pago cancelado", "Error");
              }
            }
            Firebase.database().ref(`updateuser/${this.state.code}/payment`).set(null)
          }
            
          
        })
      
      Factura.MercadoPago({dni, productsSelect, code:this.state.code}).then(data=>{
        this.setState({urlMercadoPago:data.url, mercadoPagoModal:true, loadingMercadoPago:false})

      })


    }
  
    generateCupo() {
      if (!this.state.invoices.length) {
        ERROR("No hay facturas selecionadas!!", "Error");
        return;
      } 
      const dni =  this.props.screenProps.user.user.dni;
      const productsSelect = this.state.invoices;
      const bon_intereses = 3;
      const importe_pagado = this.state.total;
      const leyendaobservada =  this.props.screenProps.user.user.codtipodoc;
      const observaciones = this.state.observaciones;
      const tipo = 'Efectivo';
      const vencimiento = this.state.vencimiento;
      this.setState({loadingCupon:true})

      Factura.GenerateCupo({
          dni,
          productsSelect, 
          bon_intereses, 
          importe_pagado, 
          leyendaobservada,
          observaciones,
          tipo, 
          vencimiento
      }).then((data)=>{
        console.log(data)
        Linking.openURL(data.url);
        this.setState({loadingCupon:false})
        this.props.navigation.navigate('Invoices');
        Firebase.database().ref(`update/${this.props.screenProps.user.user['id']}`).set({updateFacturas:true})

      })
      .catch(error=>{
        this.setState({loadingCupon:false})
        ERROR("Error registrando factura.", "Error");
      })

    }
    render() {
      

        return (
            <Container>
                <Header style={{backgroundColor:'#2CA8FF'}}>
                <Left>
                    <Button onPress={()=>this.props.navigation.goBack()} transparent>
                      <Icon name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                   <Title>Pagar facturas</Title>
                </Body>
                <Right>
                </Right>
                </Header>
                <Content>
                  <View style={{flexDirection:'row',justifyContent:'center',paddingTop:20}}>
                    <Text>{`${this.props.screenProps.user.user.nombre} `}</Text>
                  </View>
                  <View style={{flexDirection:'row',justifyContent:'center',marginTop:-25}}>
                    <Text style={styles.eventoFecha}>
                      {"\n"}
                      {`${this.state.tipodoc}: ${this.state.dni}`}
                    </Text>
                  </View>
                  <View style={{flexDirection:'row',justifyContent:'center',paddingBottom:30}}>
                    <Text style={styles.eventoFecha}>
                      {'Factura:  ' + this.state.tipofactura} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      {'IVA:  ' + this.state.condicioniva} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      {'Fecha:  ' + this.state.vencimiento}
                    </Text>
                  </View>
                  <View style={{flexDirection:'row',flex:1}}>
                    <View style={{flex:0.6, backgroundColor:'rgba(255, 255, 255, 0.3)',paddingLeft:20,paddingRight:20,}}>
                      <Text>Facturas</Text>
                      <Select
                        placeholder={{
                          label: 'Seleccione',
                          value: null,
                        }}
                        items={this.state.unselected.map(data=>({label:data['descripcion'], value:data, color:'black'}))}
                        onValueChange={(value) => {if(!value)return;this.setState({selectedValue:value, flag:true})}}
                      />
                    </View>
                    <View style={{ flex:0.4,justifyContent:'center',paddingLeft:20,paddingRight:20,paddingTop:10}}>
                      <TouchableOpacity
                        style={[styles.buttonNext, this.state.flag ? {} : {backgroundColor:'grey'}]}
                        onPress={() => this.state.flag ? this.handleSelect() : ERROR("Seleccione una factura","Error")}
                        >
                        <Text style={styles.buttonTextModal}>{'Agregar'}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                    
                  <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff',marginTop:'5%'}}>
                    <Row data={this.state.head} style={styles.head} textStyle={styles.text}/>
                    <Rows data={this.state.invoices.map((data,i)=>([data.descripcion.substr(0,10),data.debe,1,data.debe,
                      <Button onPress={() => this.handleDelete(data,i)} transparent>
                        <Icon type="Octicons" style={{color:'red',fontSize:14}} name='x' />
                      </Button>]))}
                    textStyle={styles.text}/>
                  <Row data={['Total','','',this.state.total,'']} style={styles.head} textStyle={styles.text}/>
                  </Table>
                  <View style={{paddingTop:30,paddingLeft:20,paddingRight:20}}>
                    <View style={{flexDirection:'row',justifyContent:'center'}}>
                      <TouchableOpacity
                        style={styles.buttonNext}
                        onPress={this.payMercadoPago}
                        >
                        {
                          this.state.loadingMercadoPago ? 
                          <ActivityIndicator style={{marginTop:6}} size="small" color="#fff"/>
                          :
                          <Text style={styles.buttonTextModal}>{'MercadoPago'}</Text>
                        }
                        
                      </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'center'}}>
                      <TouchableOpacity
                        style={styles.buttonNext}
                        onPress={this.generateCupo}
                        >
                        {
                          this.state.loadingCupon ? 
                          <ActivityIndicator style={{marginTop:6}} size="small" color="#fff"/>
                          :
                          <Text style={styles.buttonTextModal}>{'Generar cupón'}</Text>
                        }
                        

                      </TouchableOpacity>
                    </View>
                  </View>
                  <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.mercadoPagoModal}
                    
                    onRequestClose={() => {
                        this.setState({mercadoPagoModal:false})
                    }}>
                    
                    <View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%'}}>
                       
                        <WebView 
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            renderLoading={()=><ActivityIndicator style={{
                                position: 'absolute',
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }} size='large' color='white'/>} 
                            startInLoadingState={true}   
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex:1,
                            }}
                            source={{uri: this.state.urlMercadoPago}}/>
                    </View>
                </Modal> 
                </Content>
            </Container>
        );
      }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,

    },

    evento: {
        paddingRight:15,
        borderRadius: 5,
        // shadowOpacity: 0.3,
        // shadowOffset: {width:0, height:0},
        // shadowColor: 'gray',
        // shadowRadius: 10,
        padding:20,
        backgroundColor: "#fff",
        // borderBottomColor: "gray",
        // borderBottomWidth: 2,
    },

    eventoTitulo : {
        fontSize: 17,
        marginBottom: 5,
        color: "#DEB749"
    },

    eventoDescripcion : {
        color: "#212121"
    },

    eventoFecha : {
        marginTop: 10,
        color: "#212121"
    },

    title : {
        fontSize: 18,
        marginBottom: 15,
        textAlign:'center'
    },

    proveedor : {
        padding: 20,
        borderBottomColor: "#ecf0f1",
        borderBottomWidth: 1,
        backgroundColor: "#fff",
    },
    head: { height: 40, backgroundColor: '#f1f8ff' },
    text: { margin: 6, fontSize:10   },
    proveedorNombre : {
        fontSize: 17,
        marginBottom: 5,
        fontWeight: 'normal',
        color: "#DEB749"
    },

    proveedorDescripcion : {

    },

    proveedorNombrePrecio: {
        flexDirection: 'row',
        justifyContent:'space-between',

    },

    proveedorPrecio : {
        fontSize: 18,
        color: "#ea1d75",
        fontWeight: 'bold'
    },

    proveedorAction : {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    buttonNext : {
        height: 40,
        backgroundColor:"#2CA8FF",
        alignSelf: 'stretch',
        width:'100%',
        marginBottom:'5%'
    },

    buttonText : {
        color: "#ea1d75",
        textAlign:'center',
        fontSize: 17,
    },

    buttonTextModal : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
        paddingVertical:10
    },
    buttonModal : {
        height: 50,
        backgroundColor:"#ea1d75",
        width:'80%',
        alignSelf: 'stretch',

        marginLeft:'10%',
        borderRadius: 3,
    },
    modal:{
        backgroundColor:'#eee',
        width:'70%',
        marginLeft:'15%',
        height:250,
        borderColor:'black',
        borderWidth:1,
        borderRadius:10,
        justifyContent:'center'

    },
    modalText:{
        color: "#000",
        textAlign:'center',
        fontSize: 17,
    },

    paypalLogo:{
        width:30,
        height:30
    }

});
