<!DOCTYPE html>
<html>
<head>
  <title>GENERACION CUPON</title>
  <style type="text/css">
    body{
      background-color:#DEDEDE;
      padding-top:20px;
      padding-bottom:20px;
      font-family:"Times New Roman";
    }
    .page1{
      width:165mm; /*210-40*/
      height:237.4mm; /*297.4-60*/
      background-color:white;
      padding:20px 20px 20px 20px;
      margin-left:auto;
      margin-right:auto;
      box-shadow: 0 0 50px #888888;
    }
    .content{
       padding:20px 20px 20px 20px;
    }

    header{height:26%;border-bottom:1px solid #000000;}
    .div-header {float:left;padding-top:8px;}

    .left-header{
      width:33%;
    }
    .right-header{
      width:33%;
    }

    .center-header{width:calc(34% - 2px);border-left:1px solid #000000; border-right:1px solid #000000;}
    section{padding-top:20px;padding-bottom:20px;}
    section{margin:0px;}
    footer{position:absolute; bottom:0;left:0;right:0;padding:20px 0px 20px 0px;} /*Para que el footer llegue hasta abajo*/
    img{margin-bottom:2px;height:85px;max-width:208;}

    .column{height: calc(100% - 8px);}
    .row{left:0;right:0;position:relative;}
    .text-center{text-align:center;}
    .text-left{text-align:left;}
    .text-right{text-align:right;}
    .negrita{font-weight:bold;}
    .preimpreso{color:#999999;text-transform:uppercase;}
    .uppercase{text-transform:uppercase;}
    .h0{font-size:30px;}
    .h1{font-size:25px;}
    .h2{font-size:20px;}
    .h3{font-size:12px;}
    .h4{font-size:10px;}
    .container{margin:0 20px 0 20px;}
    .bordeRecibo{border:1px solid #000000;margin:0;padding:0;height:100%;position:relative;} /*Para que el footer llegue hasta abajo*/
    .pull-right{position:absolute;right:0;}

    #logo{content: url('/images/recibo/logo1300x600-TDFSATELITAL.png');}

    #tipoComprobante{font-size:50px;}
    #leyendaTipoComprobante{font-size:10px;}
    #tipoComprobante, #lblComprobante{line-height:70%;}
    #lblNroCmp{line-height:200%;}
    #hr{width:30%;border-top:1px solid #000000;height:1px;position:absolute;right:20px;}
    #firma{padding-top:30mm;position:relative;}

    .importeEnPesos:before{content:'$';}

    @media screen{
      body{background-color:#DEDEDE;}
      #page1{box-shadow: 0 0 50px #888888;padding:20mm 18mm 18mm 20mm;}
    }
    @media print{
      body{background-color:#FFFFFF;/*font-family:"Verdana";*/}
      #page1{box-shadow:none;padding:0;}
      button {display:none;}
      #botonera{display:none;}
    }


    /*BOTONERA*/
    button{
      color:#FFFFFF;
      background-color:#428BCA;
      border: 1px solid #357EBD;
      padding:6px 12px;
      margin-bottom:5px;
      text-align: center;
      cursor: pointer;
      font-weight:bold;
    }

    #botonera{
      z-index:999;
      position:absolute;
      left:10px;
      top:10px;
    }
  </style>
</head>

<body>

	<div id="botonera">
    <button onClick="javascript:window.print();">Imprimir</button>
  </div>

  <div id="page1" class="page1">

    <div class="bordeRecibo">

      <header>

        <!-- Lado Izquierdo -->
        <div class="column left-header div-header">
          <div class="row text-center negrita" style="font-size:13px; padding-bottom:5px">BANCO DE TIERRA DEL FUEGO ENTE RECAUDADOR</div>
          <div style="width:100%; height:1px; background-color:#616A6B"></div>
          <div style="padding-left:5px">
            <div class="row h3 negrita">Cuenta:</div>
            <div class="row h2"><span class="preimpreso">003030399020</span></div>
            <div class="row h3"><span class="negrita">Sucursal: </span>&nbsp;&nbsp;03</div>
            <div class="row h3"><span class="negrita">Cliente: </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="uppercase">{{$cliente}}</span></div>
            <div class="row h3"><span class="negrita">DNI: </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$dni}}</div>
            <div class="row h3"><span class="negrita">Importe: </span>&nbsp;&nbsp;&nbsp;<span class="negrita" style="font-size:15px">$ {{$importe}}.-</span></div>
            <div class="row h3"><span class="negrita">Convenio: </span>&nbsp;0000001</div>
          </div>
          <div class="row text-center negrita h0 preimpreso">003030399020</div>
          <div class="row text-center negrita h3" style="padding-top:8px" >Talón Banco BTF</div>
        </div>

        <!-- Centro -->
        <div class="column center-header div-header">
          <div class="row text-center negrita" style="font-size:13px; padding-bottom:5px">BANCO DE TIERRA DEL FUEGO ENTE RECAUDADOR</div>
          <div style="width:100%; height:1px; background-color:#616A6B"></div>
          <div style="padding-left:5px">
            <div class="row h3 negrita">Cuenta:</div>
            <div class="row h2"><span class="preimpreso">003030399020</span></div>
            <div class="row h3"><span class="negrita">Sucursal: </span>&nbsp;&nbsp;03</div>
            <div class="row h3"><span class="negrita">Cliente: </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="uppercase">{{$cliente}}</span></div>
            <div class="row h3"><span class="negrita">DNI: </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$dni}}</div>
            <div class="row h3"><span class="negrita">Importe: </span>&nbsp;&nbsp;&nbsp;<span class="negrita" style="font-size:15px">$ {{$importe}}.-</span></div>
            <div class="row h3"><span class="negrita">Convenio: </span>&nbsp;0000001</div>
          </div>
          <div class="row text-center negrita h0 preimpreso">003030399020</div>
          <div class="row text-center negrita h3" style="padding-top:8px" >Talón TDFSatelital</div>
        </div>

        <!-- Lado Derecho -->
        <div class="column right-header div-header">
          <div class="row text-center negrita" style="font-size:13px; padding-bottom:5px">BANCO DE TIERRA DEL FUEGO ENTE RECAUDADOR</div>
          <div style="width:100%; height:1px; background-color:#616A6B"></div>
          <div style="padding-left:5px">
            <div class="row h3 negrita">Cuenta:</div>
            <div class="row h2"><span class="preimpreso">003030399020</span></div>
            <div class="row h3"><span class="negrita">Sucursal: </span>&nbsp;&nbsp;03</div>
            <div class="row h3"><span class="negrita">Cliente: </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="uppercase">{{$cliente}}</span></div>
            <div class="row h3"><span class="negrita">DNI: </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$dni}}</div>
            <div class="row h3"><span class="negrita">Importe: </span>&nbsp;&nbsp;&nbsp;<span class="negrita" style="font-size:15px">$ {{$importe}}.-</span></div>
            <div class="row h3"><span class="negrita">Convenio: </span>&nbsp;0000001</div>
          </div>
          <div class="row text-center negrita h0 preimpreso">003030399020</div>
          <div class="row text-center negrita h3" style="padding-top:8px" >Talón Cliente</div>
        </div>
      </header>
    </div><!-- bordeRecibo -->
  </div><!-- Page1 -->
</body>
</html>
