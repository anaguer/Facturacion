<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reclamos extends Model
{
	/**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'reclamos';

    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    "id",
		"descripcion",
		"personales_dni",
		"facturas_id_factura"
	];

	public function reclamo()
    {
    	return $this->belongsTo('App\Factura', 'facturas_id');
    }
}
