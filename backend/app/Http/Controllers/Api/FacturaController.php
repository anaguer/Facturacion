<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Mpdf\Mpdf;
use App\Factura;
use App\Client;
use App\Product;
use App\Recibo;
use JWTAuth;

use App\Facturas_has_articulo_det;
use Carbon\Carbon;
use COM;
class FacturaController extends Controller
{

    public function getGenerar(Request $request)
    {
        $accion = $request->get('accion');
        $tipo = $request->get('tipo');
        $id = $request->get('id');
        return $this->pdf($accion,$tipo, $id);
    }

    public function pdf($accion,$tipo, $id)
    {
    	$factura = Factura::where('cae',$id)->get();
    	//return $factura;
    	$client = Client::where('id', $factura[0]->codcliente)->first();
    	//return $client;

    	$ruc = "10072486893";
        $numero = $factura[0]->num_factura;
        $metodoPago = $factura[0]->tipo;
        $nombres = $client->nombre;
        $dates = [];
        if (strpos($factura[0]->fecha_sistema, '-') !== false) {
        	 $dates[0] = substr($factura[0]->fecha_sistema, 8,2);
        	 $dates[1] = substr($factura[0]->fecha_sistema, 5,2);
        	 $dates[1] = substr($factura[0]->fecha_sistema, 0,4);
        } else {
        	 $dates =  explode('/', $factura[0]->fecha_sistema);
        }


        $direccion = $client->domicilio;
        $dni = $client->dni;
        $total = 0;

         $articulos = [];
        foreach ($factura as $key => $value) {

        	array_push($articulos, [
        		"cantidad" => 1,
        		"descripcion" => $value->descripcion,
        		"precio" => $value->debe,
        		"importe" => ((int)$value->debe)*1,
        		"cod_producto" => $value->cod_producto,
        	]);
        }

        foreach ($articulos as $key => $value) {
            $total += $value["importe"];
            $articulos[$key]["precio"] = (int)$value["precio"];
            $articulos[$key]["importe"] = (int)$value["importe"];
            $articulos[$key]["cod_producto"] = $value["cod_producto"];

        }
        $total = number_format($total,2,'.','');


        $data['ruc'] = $ruc;
        $data['numero'] = $numero;
        $data['nombres'] = $nombres;
        $data['dia'] = $dates[0];
        $data['mes'] = $dates[1];
        $data['ayo'] = $dates[2];
        $data['direccion'] = $direccion;
        $data['dni'] = $dni;
        $data['tipo_factura'] = $client->tipofactura;
        $data['articulos'] = $articulos;
        $data['total'] = $total;
        $data['tipo'] = $tipo;
        $data['metodoPago'] = $metodoPago;
        $data['Cae'] = $id;
        $data['localidad'] = $client->ciudad;
       	$data['condicioniva'] = $client->condicioniva;
       	$data['condicionventa'] = $client->comopaga;


        if($accion=='html'){
            return view('facturacion.index',$data);
        }else{
            $html = view('facturacion.index',$data)->render();
        }

        $namefile = 'boleta_de_venta_'.time().'.pdf';

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                public_path() . '/fonts',
            ]),
            'fontdata' => $fontData + [
                'arial' => [
                    'R' => 'sueca-medium.ttf',
                    'B' => 'sueca-bold.ttf',
                ],
            ],
            'default_font' => 'arial',
            // "format" => "A4",
            "format" => "A4",
        ]);

        // $mpdf->SetTopMargin(5);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        // dd($mpdf);
        if($accion=='ver'){
            $mpdf->Output($namefile,"I");
        }else if($accion=='descargar'){
            $mpdf->Output($namefile,"D");
        }

    }

    public function Create(Request $request) {

    	$factura = new Factura;
		$factura->id = Factura::all()->count()+1;

		if($request->codcliente )
			$factura->codcliente = $request->codcliente;
		if($request->codplan )
			$factura->codplan = $request->codplan;
		if($request->dni )
			$factura->dni = $request->dni;
		if($request->nombre )
			$factura->nombre = $request->nombre;
		if($request->cod_producto )
			$factura->cod_producto = $request->cod_producto;
		if($request->num_factura )
			$factura->num_factura = $request->num_factura;
		if($request->descripcion )
			$factura->descripcion = $request->descripcion;
		if($request->vencimiento )
			$factura->vencimiento = $request->vencimiento;
		if($request->mora )
			$factura->mora = $request->mora;
		if($request->vencimiento2 )
			$factura->vencimiento2 = $request->vencimiento2;
		if($request->debe )
			$factura->debe = $request->debe;
		if($request->saldo )
			$factura->saldo = $request->saldo;
		if($request->observada )
			$factura->observada = $request->observada;
		if($request->leyendaobservada )
			$factura->leyendaobservada = $request->leyendaobservada;
		if($request->clase )
			$factura->clase = $request->clase;
		if($request->haber )
			$factura->haber = $request->haber;
		if($request->hora )
			$factura->hora = $request->hora;
		if($request->fecha_sistema )
			$factura->fecha_sistema = $request->fecha_sistema;
		if($request->cod_mes )
			$factura->cod_mes = $request->cod_mes;
		if($request->pagado )
			$factura->pagado = $request->pagado;
		if($request->periodo )
			$factura->periodo = $request->periodo;
		if($request->fecha_pago )
			$factura->fecha_pago = $request->fecha_pago;
		if($request->importe_pagado )
			$factura->importe_pagado = $request->importe_pagado;
		if($request->recibo )
			$factura->recibo = $request->recibo;
		if($request->codoperario )
			$factura->codoperario = $request->codoperario;
		if($request->forma )
			$factura->forma = $request->forma;
		if($request->estado )
			$factura->estado = $request->estado;
		if($request->entrega )
			$factura->entrega = $request->entrega;
		if($request->bon_intereses )
			$factura->bon_intereses = $request->bon_intereses;
		if($request->observaciones )
			$factura->observaciones = $request->observaciones;
		if($request->pagosraul )
			$factura->pagosraul = $request->pagosraul;
		if($request->fechapagoraul )
			$factura->fechapagoraul = $request->fechapagoraul;
		if($request->reciboraul )
			$factura->reciboraul = $request->reciboraul;
		if($request->sergiopagado )
			$factura->sergiopagado = $request->sergiopagado;
		if($request->observacionessergio )
			$factura->observacionessergio = $request->observacionessergio;
		if($request->codenviado )
			$factura->codenviado = $request->codenviado;
		if($request->ZONA )
			$factura->ZONA = $request->ZONA;
		if($request->tipo )
			$factura->tipo = $request->tipo;
		if($request->cae )
			$factura->cae = $request->cae;
		if($request->caevencimiento )
			$factura->caevencimiento = $request->caevencimiento;
		if($request->caeentregado )
			$factura->caeentregado = $request->caeentregado;




		/*$recibo = new Recibo;

    $recibo->fecha = Carbon::now();
		$recibo->descripcion = "Primer pago fatura #{$factura->id}" ;
		$recibo->facturas_id = $factura->id;
		$recibo->facturas_dni = $factura->dni;
		$recibo->importe = $factura->pagado;
    if ($request->tipo == 1) {
      $recibo->forma = "Efectivo";
    } elseif ($request->tipo == 2) {
      $recibo->forma = "Tarjeta de Crédito";
    } elseif ($request->tipo == 3) {
      $recibo->forma = "Cuotas";
    }
		$recibo->importe_original = $factura->importe_pagado;
		$recibo->save();
	*/
		DB::insert('insert into facturas
			(
				id_factura,
				codcliente,
				codplan,
				dni,
				nombre,
				cod_producto,
				num_factura,
				descripcion,
				vencimiento,
				mora,
				vencimiento2,
				debe,
				saldo,
				observada,
				leyendaobservada,
				clase,
				haber,
				hora,
				fecha_sistema,
				cod_mes,
				pagado,
				periodo,
				fecha_pago,
				importe_pagado,
				recibo,
				codoperario,
				forma,
				estado,
				entrega,
				bon_intereses,
				observaciones,
				pagosraul,
				fechapagoraul,
				reciboraul,
				sergiopagado,
				observacionessergio,
				codenviado,
				ZONA,
				tipo,
				cae,
				caevencimiento,
				caeentregado
			) values (
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				?
			)',
			[
				$factura->id_factura,
				$factura->codcliente,
				$factura->codplan,
				$factura->dni,
				$factura->nombre,
				$factura->cod_producto,
				$factura->num_factura,
				$factura->descripcion,
				$factura->vencimiento,
				$factura->mora,
				$factura->vencimiento2,
				$factura->debe,
				$factura->saldo,
				$factura->observada,
				$factura->leyendaobservada,
				$factura->clase,
				$factura->haber,
				$factura->hora,
				$factura->fecha_sistema,
				$factura->cod_mes,
				$factura->pagado,
				$factura->periodo,
				$factura->fecha_pago,
				$factura->importe_pagado,
				$factura->recibo,
				$factura->codoperario,
				$factura->forma,
				'0',
				$factura->entrega,
				$factura->bon_intereses,
				$factura->observaciones,
				$factura->pagosraul,
				$factura->fechapagoraul,
				$factura->reciboraul,
				$factura->sergiopagado,
				$factura->observacionessergio,
				$factura->codenviado,
				$factura->ZONA,
				$factura->tipo,
				$factura->cae,
				$factura->caevencimiento,
				$factura->caeentregado
			]);

		$factura = Factura::all()->last();
		foreach ($request->productsSelect as $key => $value) {;
			$product = new Facturas_has_articulo_det;
			$product->facturas_id = $factura->id;
			$product->facturas_personales_id = $request->dni;
			$product->articulo_det_id = $value["id_ar"];
			$product->cantidad = $value["cantidad"];
			DB::insert('insert into facturas_has_articulo_det
			(
				facturas_id,
				facturas_personales_id,
				articulo_det_id,
				cantidad
			) values (
				?,
				?,
				?,
				?
			)',
			[
				$product->facturas_id,
				$product->facturas_personales_id,
				$product->articulo_det_id,
				$product->cantidad
			]);
		}

		return $factura;
    }

    public function Edit(Request $request)
    {
    	$factura = [];


		if($request->codcliente )
			$factura['codcliente'] = $request->codcliente;
		if($request->codplan )
			$factura['codplan'] = $request->codplan;
		if($request->dni )
			$factura['dni'] = $request->dni;
		if($request->nombre )
			$factura['nombre'] = $request->nombre;
		if($request->cod_producto )
			$factura['cod_producto'] = $request->cod_producto;
		if($request->num_factura )
			$factura['num_factura'] = $request->num_factura;
		if($request->descripcion )
			$factura['descripcion'] = $request->descripcion;
		if($request->vencimiento )
			$factura['vencimiento'] = $request->vencimiento;
		if($request->mora )
			$factura['mora'] = $request->mora;
		if($request->vencimiento2 )
			$factura['vencimiento2'] = $request->vencimiento2;
		if($request->debe )
			$factura['debe'] = $request->debe;
		if($request->saldo )
			$factura['saldo'] = $request->saldo;
		if($request->observada )
			$factura['observada'] = $request->observada;
		if($request->leyendaobservada )
			$factura['leyendaobservada'] = $request->leyendaobservada;
		if($request->clase )
			$factura['clase'] = $request->clase;
		if($request->haber )
			$factura['haber'] = $request->haber;
		if($request->hora )
			$factura['hora'] = $request->hora;
		if($request->fecha_sistema )
			$factura['fecha_sistema'] = $request->fecha_sistema;
		if($request->cod_mes )
			$factura['cod_mes'] = $request->cod_mes;
		if($request->pagado )
			$factura['pagado'] = $request->pagado;
		if($request->periodo )
			$factura['periodo'] = $request->periodo;
		if($request->fecha_pago )
			$factura['fecha_pago'] = $request->fecha_pago;
		if($request->importe_pagado )
			$factura['importe_pagado'] = $request->importe_pagado;
		if($request->recibo )
			$factura['recibo'] = $request->recibo;
		if($request->codoperario )
			$factura['codoperario'] = $request->codoperario;
		if($request->forma )
			$factura['forma'] = $request->forma;
		if($request->estado )
			$factura['estado'] = $request->estado;
		if($request->entrega )
			$factura['entrega'] = $request->entrega;
		if($request->bon_intereses )
			$factura['bon_intereses'] = $request->bon_intereses;
		if($request->observaciones )
			$factura['observaciones'] = $request->observaciones;
		if($request->pagosraul )
			$factura['pagosraul'] = $request->pagosraul;
		if($request->fechapagoraul )
			$factura['fechapagoraul'] = $request->fechapagoraul;
		if($request->reciboraul )
			$factura['reciboraul'] = $request->reciboraul;
		if($request->sergiopagado )
			$factura['sergiopagado'] = $request->sergiopagado;
		if($request->observacionessergio )
			$factura['observacionessergio'] = $request->observacionessergio;
		if($request->codenviado )
			$factura['codenviado'] = $request->codenviado;
		if($request->ZONA )
			$factura['ZONA'] = $request->ZONA;
		if($request->tipo )
			$factura['tipo'] = $request->tipo;
		if($request->cae )
			$factura['cae'] = $request->cae;
		if($request->caevencimiento )
			$factura['caevencimiento'] = $request->caevencimiento;
		if($request->caeentregado )
			$factura['caeentregado'] = $request->caeentregado;
		Facturas_has_articulo_det::where('facturas_id', $request->id)->delete();

		foreach ($request->productsSelect as $key => $value) {;
			$product = new Facturas_has_articulo_det;
			$product->facturas_id = $request['id'];
			$product->facturas_personales_id = $request->dni;
			$product->articulo_det_id = $value["id_ar"];
			$product->cantidad = $value["cantidad"];
			DB::insert('insert into facturas_has_articulo_det
			(
				facturas_id,
				facturas_personales_id,
				articulo_det_id,
				cantidad
			) values (
				?,
				?,
				?,
				?
			)',
			[
				$product->facturas_id,
				$product->facturas_personales_id,
				$product->articulo_det_id,
				$product->cantidad
			]);
		}


		Factura::where('id', $request->id)->update($factura);

		return Factura::where('id', $request->id)->first();
    }

    public function Delete(Request $request)
    {


        $factura = Factura::where('id', $request->id)->first();
        $factura->delete();
        return $factura;
    }

    public function All(Request $request)
    {
		if($request->page === 'undefined'){
			if($request->search === 'undefined') {
				$total = Factura::all()->count();
		        $i = 0;
		        $data = [];
		        $users =Factura::orderBy('estado','desc')->get();
		        foreach ( $users as $key => $value) {

		          	foreach ($value->getAttributes() as $key2=> $data2) {
		              $value[$key2]= utf8_encode($value[$key2]);

		              }
		            array_push($data, $value);

		          $i++;
		      	}

		    	return $data;
			}
          $i = 0;
          $data = [];
          $users = Factura::orderBy('estado','desc')->where($request->search, $request->value)->get();
          foreach ( $users as $key => $value) {
            foreach ($value->getAttributes() as $key2=> $data2) {
              $value[$key2]= utf8_encode($value[$key2]);

              }
            array_push($data, $value);
          }
          return $data;
        }

		if($request->search === 'undefined') {
			$total = Factura::all()->count();
	        $i = 0;
	        $data = [];
	        $users =Factura::orderBy('estado','desc')->get();
	        foreach ( $users as $key => $value) {
	          if ($i < ((((int)$request->page)-1)*5) + 5 && $i >= ((((int)$request->page)-1)*5)  ) {
	          	foreach ($value->getAttributes() as $key2=> $data2) {
	              $value[$key2]= utf8_encode($value[$key2]);

	              }
	            array_push($data, $value);

	          }
	           $i++;

	      	}
	      	return response()->json(['data'=>$data , 'total'=>$total], 200, [], JSON_UNESCAPED_UNICODE);

		}


		$total = Factura::where($request->search, $request->value)->count();
        $i = 0;
        $data = [];
        $users =Factura::where($request->search, $request->value)->orderBy('estado','desc')->get();
	        foreach ( $users as $key => $value) {
	          if ($i < ((((int)$request->page)-1)*5) + 5 && $i >= ((((int)$request->page)-1)*5)  ) {
	          	foreach ($value->getAttributes() as $key2=> $data2) {
	              $value[$key2]= utf8_encode($value[$key2]);

	              }
	            array_push($data, $value);
	          }
	          $i++;
      	}

    	return ['data'=>$data , 'total'=>$total];


    }



    public function Factura($id)
    {
    	return Factura::where('id', $id)->first();
    }

    public function IvaClients(){
    	return Client::where('condicioniva','')->update(['condicioniva'=>'0%']);
    }


    public function FacturaForClient($dni){
      return Factura::where("dni", $dni)->get();
	}

  public function clientInvoices($dni){
    return Factura::where("dni", $dni)->get();
  }

	public function ProductsFacturas(Request $request)
	{
		$facturaProducto = Facturas_has_articulo_det::where('facturas_id', $request->id)->get();
		$result = [];
		if ($facturaProducto) {
			for ($i=0; $i < count($facturaProducto); $i++) {
				$products = Product::where('id_ar',$facturaProducto[$i]->articulo_det_id)->first();
				$products->cantidad = $facturaProducto[$i]->cantidad;
				array_push($result, $products);
			}
		}
		return $result;
	}


	function verInformacionAfip($factura){
		$voucher_info = $this->_afip->ElectronicBilling->GetVoucherInfo($factura->id,1,6); //Devuelve la información del comprobante 1 para el punto de venta 1 y el tipo de comprobante 6 (Factura B)

		if($voucher_info === NULL){
		    echo 'El comprobante no existe';
		}
		else{
		    echo 'Esta es la información del comprobante:';
		    echo '<pre>';
		    print_r($voucher_info);
		    echo '</pre>';
		}
	}

  public function coupon(){
    $dni = request('dni');
    $cliente = Client::where('dni', $dni)->first();
    $importe = request('importe') ;

    return $this->printCoupon($cliente->nombre, $dni, $importe);
  }

  public function generateCoupon(Request $request){
  	$data = [];
  	$data['url'] = "https://facturacionblue.000webhostapp.com/api/coupon?importe=".$request->importe_pagado."&dni=".$request->dni;
  	return $data;
  }

  public function printCoupon($cliente, $dni, $importe){
    $data['cliente'] = $cliente;
    $data['dni'] = $dni;
    $data['importe'] = $importe;
    return view('cupon.index', $data);
  }
}
