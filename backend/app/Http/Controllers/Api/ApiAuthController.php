<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Client;
use App\Profile;

class ApiAuthController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $users = User::all();

        foreach ($users as $key => $value) {
          if ($value['email'] == request('email') && $value['dni'] == request('dni')) {
            continue;
          } elseif ($value['email'] == request('email')) {
            return response()-> json(['error' => 'Este e-mail ya se encuentra registrado.']);
          }
        }


        $user = [];
        if($request->usuario != null)
            $user['usuario']= $request->usuario;

        if($request->nombre != null)
            $user['nombre']= $request->nombre;
        if($request->password != null)
            $user['clave']= $request->password;

        User::where('id', $request->id)->update($user);
        $user = User::where('id', $request->id)->first();

        $token = JWTAuth::fromUser($user);
        return $this->respondWithToken($token, $user);
    }


    public function getUsers(Request $request)
    {

        if($request->page === 'undefined'){
            return User::get();
          $i = 0;
          $data = [];
          $users = User::all();
          foreach ( $users as $key => $value) {
            foreach ($value->getAttributes() as $key2=> $data2) {
              $value[$key2]= utf8_encode($value[$key2]);
              error_log($value[$key2]);
              }                           
            array_push($data, $value);
          }
          return $data;
        }

        $total = User::all()->count();
        $i = 0;
        $data = [];
        $users = User::all();
        foreach ( $users as $key => $value) {
          if ($i < ((((int)$request->page)-1)*5) + 5 && $i >= ((((int)$request->page)-1)*5)  ) {
            array_push($data, $value);
          }
          $i++;
        }
    	return ['data'=>$data , 'total'=>$total];
    }


    public function deleteUsers(Request $request)
    {


        $user = User::find($request->id);
        $user->delete();
        return response()->json(['user' => $user], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Update(Request $request)
    {

        $user->usuario = $request->usuario;
        $user->token = $request->token;
        $user->payment = $request->payment;

        $user->roles_idroles = $request->roles_idroles;
        $user->payment = $request->payment;
        $user->save();

        $profile = Profile::where('user_id', $user->id);
        $profile->name = $request->name;
        $profile->description = $request->description;
        $profile->rating = $request->rating;

        $user->Profile()->save($profile);


        $token = JWTAuth::fromUser($user);
        return $this->respondWithToken($token);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function authenticate(Request $request)
    {
        $user = User::select('*')->where('email', '=', $request->email)->first();

        $credentials = $request->only('email', 'clave');

        try
        {
            $token = JWTAuth::attempt($credentials);

            if(!$token)
            {
                return response()->json(['error' => 'Datos incorrectos'], 401);
            }
        }
        catch(JWTException $e)
        {
            return response()->json(['error' => 'Ocurrio un error'], 500);
        }

        return response()->json(['token' => $token], 200);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'clave');
        $user = User::where('usuario', $request->email)->where('clave', $request->password)->first();
        if ($user != null) {
            $token = JWTAuth::fromUser($user);

            return $this->respondWithToken($token, $user);
        }

        return response()->json(['error' => 'No Autorizado'], 401);
    }

    public function clientLogin(Request $request)
    {
        $cliente = Client::where('email', $request->email)->where('dni', $request->password)->first();
        if ($cliente != null) {
          return $cliente;
        }

        return response()->json(['error' => 'No Autorizado'], 401);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Sesión Finalizada con exito']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $user)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 60,
            'user'=>$user

        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {

       try {
            $user = JWTAuth::parseToken()->toUser();
       } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
       }


       return response()->json(['user'=> $user]);
    }

    public function register(Request $request)
    {

      $users = User::all();

      foreach ($users as $key => $value) {
        if ($value['usuario'] == request('usuario')) {
          return response()-> json(['error' => 'Este usuario ya se encuentra registrado.']);
        }
      }

        $user = new User;

        if ($request->usuario)
            $user->usuario= $request->usuario;
        if ($request->administrador)
            $user->administrador= $request->administrador;

        if($request->nombre != null)
            $user->nombre = $request->nombre;
        if($request->password != null)
            $user->clave = $request->password;


        try {
            DB::insert('insert into usuarios (usuario, nombre, clave, administrador) values (?, ?, ?, ?)', [$user->usuario, $user->nombre, $user->clave, $user->administrador]);
        } catch (Exception $e) {
           return Response::json(['error' => 'User already exists.'], HttpResponse::HTTP_CONFLICT);
        }

        $user  = User::all()->last();
        $token = JWTAuth::fromUser($user);
        return $this->respondWithToken($token, $user);
    }

}
