<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MP;
use App\Factura;
use App\Facturas_has_articulo_det;
use App\Client;
use App\Product;
use App\Recibo;
use App\Afip;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;


class ApiMercadoPagoController extends Controller
{
    public function __construct()
    {
       $this->_afip = new Afip(array('CUIT' => 20246889350)); 
    }
    
   


    public function checkout(Request $request)
    {

       
        $client = Client::where('dni',$request->dni)->first();

        $articulos = [];
        $reference = '';
        
        
        $preferenceData = [
            
            'payer'              => [
                'phone' => array(
                    "area_code" => "54",
                    "number" => $client->telefono
                ),
                'address' => array(
                    "street_name" => $client->Barrio,
                    "street_number" => $client->cod_barrio,
                ),
                'email' => $client->email,
                'name' => $client->nombre,
                'surname' => $client->ap,
            ],
            
            'notification_url'  => 'http://facturacionblue.000webhostapp.com/api/Ipc'
        ];
       
       foreach ($request->productsSelect as $key => $value) {
            $reference .= $value['id'].'-';
            $preferenceData['items'][] = [
                'id'          => $value['id'],
                'title'       => $value['descripcion'],            
                'description' => $value['descripcion'],
                'quantity'    => 1,
                'currency_id' => 'ARS',
                'unit_price'  => (int)$value['debe'],
            ];
        }
           
        $preferenceData['external_reference'] = $reference;
        $preferenceData['back_urls'] = [
                'success' => 'https://facturacionblue.000webhostapp.com/api/IpcSuccess/'.$request->code.'/'.$reference,
                'pending' => 'https://facturacionblue.000webhostapp.com/api/IpcPending/'.$request->code.'/'.$reference,
                'failure' => 'https://facturacionblue.000webhostapp.com/api/IpcFailure/'.$request->code.'/'.$reference,
            ];
        // add items
        //return $preferenceData;
        $mp = new MP ('4636581455442630', '3fmlsJqT8iXcv9Q8TKg4myrbRGfJtuFI');
        $preference = $mp->create_preference($preferenceData);

        // return init point to be redirected
        
        return ['url'=>$preference['response']['init_point']];
        
    }

    public function Pending($id, $reference)
    {
        $ids = explode('-', $reference);
        foreach ($ids as $key => $value) {
            if($value != '')
                Factura::where('id', $value)->update(['estado'=>'1']);                
        }
         $this->updateDataUser($id,true);
        return redirect('/facturas');

    }

    public function updateDataUser($id, $value)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/facturacion-14607-firebase-adminsdk-xd23f-cca6a015f2.json');
 
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://facturacion-14607.firebaseio.com/')
        ->create();
        
        $database = $firebase->getDatabase();
        $newPost = $database
        ->getReference("updateuser/{$id}/payment")
        ->set([
            'value' => $value
        ]);
    }


    public function Success($id, $reference)
    {
        $ids = explode('-', $reference);
        foreach ($ids as $key => $value) {
            if($value != '')
                Factura::where('id', $value)->update(['estado'=>'1']);                
        }
         $this->updateDataUser($id,true);
        
       
        return redirect('/facturas');

    }



    public function Failed($id)
    {
        $this->updateDataUser($id,false);
        return redirect('/facturas');
    }

    public function Ipc()
    {
        $mp = new MP (env('MP_CLIENT_ID'), env('MP_CLIENT_SECRET'));

        if ( ! isset($_GET["id"], $_GET["topic"]) || ! ctype_digit($_GET["id"])) {
            abort(404);
        }

        // Get the payment and the corresponding merchant_order reported by the IPN.
        if ($_GET["topic"] == 'payment') {
            $payment_info = $mp->get("/collections/notifications/" . $_GET["id"]);
            $merchant_order_info = $mp->get("/merchant_orders/" . $payment_info["response"]["collection"]["merchant_order_id"]);
            
            // Get the merchant_order reported by the IPN.

            // get order and link the notification id
            $external_reference_id = $merchant_order_info["response"]["external_reference"];
           
           // get order
            
            
            $estado = '0';
            
            // link notification id
           

            if ($merchant_order_info["status"] == 200) {
                // If the payment's transaction amount is equal (or bigger) than the merchant_order's amount you can release your items
                
                $paid_amount = 0;

                foreach ($merchant_order_info["response"]["payments"] as $payment) {
                    $factura->status = $payment['status'];
                    if ($payment['status'] == 'approved') {
                        $paid_amount += $payment['transaction_amount'];
                    }
                }

                if ($paid_amount >= $merchant_order_info["response"]["total_amount"]) {
                    if (count($merchant_order_info["response"]["shipments"]) > 0) { 
                        
                        // The merchant_order has shipments
                        if ($merchant_order_info["response"]["shipments"][0]["status"] == "ready_to_ship") {
                            print_r("Totally paid. Print the label and release your item.");
                            $estado = '1';
                        }
                    } else {
                        // The merchant_order don't has any shipments
                        print_r("Totally paid. Release your item.");
                        $estado = '1';
                    }
                } else {
                    print_r("Not paid yet. Do not release your item.");
                    
                }
            }
            
            if ($factura->estado == '1') {
                $ids = explode('-', $external_reference_id);
                $productsSelect = [];
                foreach ($ids as $key => $value) {
                    if($value != '')
                        $productsSelect = array_push($productsSelect, Factura::where('id',$value)->first());
                    

                }
                $this->generarAfip($productsSelect);
            }
            

        }

        return response()->json(['status' => 'success'], 200);
    }

    public function getIva()
    {
        
        return  $this->_afip->ElectronicBilling->GetAliquotTypes();
    }

    public function getTipoFactura()
    {
        
        return $this->_afip->ElectronicBilling->GetVoucherTypes();

    }

    public function getNumberFactura()
    {
        return ['num' => $this->_afip->ElectronicBilling->GetLastVoucher(05,6) + 1];
    }

    public function getDataNewFactura()
    {
        


        return [
            'num' => $this->_afip->ElectronicBilling->GetLastVoucher(05,6) + 1,
            'typeDocument' => $this->_afip->ElectronicBilling->GetDocumentTypes(),
            'iva' => $this->_afip->ElectronicBilling->GetAliquotTypes(),
            'typeFactura' =>  $this->_afip->ElectronicBilling->GetVoucherTypes(),
        ];
    }

    public function RegisterPost(Request $request)
    {
        
        $ivas = $this->getIva();

        $porcentaje = 0;

        for ($i=0; $i < count($ivas); $i++){
            if((int)$ivas[$i]->Id == $request->bon_intereses){
                $porcentaje = str_replace('%', '', ($ivas[$i]->Desc));
            }
        }

        $fechas = explode('-', $request->vencimiento);



        $num = $this->getNumberFactura()['num'];
        //Crear y asignar CAE a un comprobante
        
        $data = array(
            'CantReg'   => 1,  // Cantidad de comprobantes a registrar
            'PtoVta'    => 05,  // Punto de venta
            'CbteTipo'  => $request->observaciones,  // Tipo de comprobante (ver tipos disponibles) 
            'Concepto'  => 3,  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo'   => (int)$request->leyendaobservada, // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
            'DocNro'    => $request->dni,  // Número de documento del comprador (0 consumidor final)
            'CbteDesde' => $num,  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
            'CbteHasta' => $num,  // Número de comprobante o numero del último comprobante en caso de ser mas de uno
            'CbteFch'   => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchServDesde'  => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchServHasta'  => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchVtoPago'    => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal'  => $request->importe_pagado , // Importe total del comprobante
            'ImpTotConc'=> 0,   // Importe neto no gravado
            'ImpNeto'   => $request->importe_pagado - ($request->importe_pagado*((int)$porcentaje)/100 ), // Importe neto gravado
            'ImpOpEx'   => 0,   // Importe exento de IVA
            'ImpIVA'    => ($request->importe_pagado*((int)$porcentaje)/100 ) ,  //Importe total de IVA
            'ImpTrib'   => 0,   //Importe total de tributos
            'MonId'     => 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
            'MonCotiz'  => 1,     // Cotización de la moneda usada (1 para pesos argentinos)
            'Iva'       => array( // (Opcional) Alícuotas asociadas al comprobante
                array(
                    'Id'        => $request->bon_intereses, // Id del tipo de IVA (5 para 21%)(ver tipos disponibles) 
                    'BaseImp'   => $request->importe_pagado - ($request->importe_pagado*((int)$porcentaje)/100 ), // Base imponible
                    'Importe'   => ($request->importe_pagado*((int)$porcentaje)/100 )// Importe 
                )
            ),
        );
        //return $data;
        try {
            $afip = $this->_afip->ElectronicBilling->CreateVoucher($data);
            $factura = [];
            $factura['cae'] = $afip['CAE'];
            $factura['caevencimiento'] = $afip['CAEFchVto'];
            $factura['num_factura'] = $num;
            if ($request->tipo == 'Efectivo'){
                $factura['estado'] = '1';
                $factura['pagado'] = '1';
            }
            $factura['forma'] = $request->tipo;
            foreach ($request->productsSelect as $key => $value) {
               
                Factura::where('id', $value['id'])->update($factura);
            }
            $factura['url'] = "https://facturacionblue.000webhostapp.com/pdf/generar?accion=ver&id=".$afip['CAE'];
            return $factura;

        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }
    }

    public function generarAfip($productsSelect){

        
        $tipoFacturas = $this->getTipoFactura();

        $client = Client::where('id',$productsSelect[0]->codcliente)->first();
        $dni = $client->dni;
        $observaciones = 1;
        foreach ($tipoFacturas as $key => $value) {
            if ($client->tipofactura == $value['Desc']) 
                $observaciones = $value['Id'];
        }
        $importe_pagado = 0;

        foreach ($productsSelect as $key => $value) {
            $importe_pagado +=  (int)$value->debe;
        }


        $leyendaobservada = $client->codtipodoc;

        $porcentaje = 0;
        $carbon = new \Carbon\Carbon();
        $date = $carbon->now()->format('Y-m-d');
        $tipo = 'Tarjeta de Credito';
        $fechas = explode('-', $date);



        $num = $this->getNumberFactura()['num'];
        //Crear y asignar CAE a un comprobante
        
        $data = array(
            'CantReg'   => 1,  // Cantidad de comprobantes a registrar
            'PtoVta'    => 05,  // Punto de venta
            'CbteTipo'  => $observaciones,  // Tipo de comprobante (ver tipos disponibles) 
            'Concepto'  => 3,  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo'   => (int)$leyendaobservada, // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
            'DocNro'    => $dni,  // Número de documento del comprador (0 consumidor final)
            'CbteDesde' => $num,  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
            'CbteHasta' => $num,  // Número de comprobante o numero del último comprobante en caso de ser mas de uno
            'CbteFch'   => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchServDesde'  => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchServHasta'  => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchVtoPago'    => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal'  => $importe_pagado , // Importe total del comprobante
            'ImpTotConc'=> 0,   // Importe neto no gravado
            'ImpNeto'   => $importe_pagado - ($importe_pagado*((int)$porcentaje)/100 ), // Importe neto gravado
            'ImpOpEx'   => 0,   // Importe exento de IVA
            'ImpIVA'    => ($importe_pagado*((int)$porcentaje)/100 ) ,  //Importe total de IVA
            'ImpTrib'   => 0,   //Importe total de tributos
            'MonId'     => 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
            'MonCotiz'  => 1,     // Cotización de la moneda usada (1 para pesos argentinos)
            'Iva'       => array( // (Opcional) Alícuotas asociadas al comprobante
                array(
                    'Id'        => 3, // Id del tipo de IVA (5 para 21%)(ver tipos disponibles) 
                    'BaseImp'   => $importe_pagado - ($importe_pagado*((int)$porcentaje)/100 ), // Base imponible
                    'Importe'   => ($importe_pagado*((int)$porcentaje)/100 )// Importe 
                )
            ),
        );
        //return $data;
        try {
            $afip = $this->_afip->ElectronicBilling->CreateVoucher($data);
            $factura = [];
            $factura['cae'] = $afip['CAE'];
            $factura['caevencimiento'] = $afip['CAEFchVto'];
            $factura['num_factura'] = $num;
            $factura['estado'] = '1';
            $factura['pagado'] = '1';
            $factura['forma'] = $tipo;
            foreach ($productsSelect as $key => $value) {
               
                Factura::where('id', $value['id'])->update($factura);
            }
            $factura['url'] = "https://http://facturacionblue.000webhostapp.com/pdf/generar?accion=ver&id=".$afip['CAE'];
            return $factura;

        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }

        
    }
    public function generarAfipWeb($id){

        $factura = Factura::where('id',$id)->first();
        if($factura->cae != ''){
            return $factura;
        }
        $ivas = $this->getIva();

        $porcentaje = 0;
        for ($i=0; $i < count($ivas); $i++){
            if((int)$ivas[$i]->Id == $factura->bon_intereses){
                $porcentaje = str_replace('%', '', ($ivas[$i]->Desc));
            }
        }

        $fechas = explode('-', $factura->vencimiento);
        $num = $this->getNumberFactura()['num'];
        //Crear y asignar CAE a un comprobante
        
        $data = array(
            'CantReg'   => 1,  // Cantidad de comprobantes a registrar
            'PtoVta'    => 05,  // Punto de venta
            'CbteTipo'  => $factura->observaciones,  // Tipo de comprobante (ver tipos disponibles) 
            'Concepto'  => 3,  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo'   => (int)$factura->leyendaobservada, // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
            'DocNro'    => $factura->dni,  // Número de documento del comprador (0 consumidor final)
            'CbteDesde' => $num,  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
            'CbteHasta' => $num,  // Número de comprobante o numero del último comprobante en caso de ser mas de uno
            'CbteFch'   => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchServDesde'  => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchServHasta'  => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'FchVtoPago'    => $fechas[0].$fechas[1].$fechas[2], // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal'  => $factura->haber , // Importe total del comprobante
            'ImpTotConc'=> 0,   // Importe neto no gravado
            'ImpNeto'   => $factura->haber - ($factura->haber*((int)$porcentaje)/100 ), // Importe neto gravado
            'ImpOpEx'   => 0,   // Importe exento de IVA
            'ImpIVA'    => ($factura->haber*((int)$porcentaje)/100 ) ,  //Importe total de IVA
            'ImpTrib'   => 0,   //Importe total de tributos
            'MonId'     => 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
            'MonCotiz'  => 1,     // Cotización de la moneda usada (1 para pesos argentinos)
            'Iva'       => array( // (Opcional) Alícuotas asociadas al comprobante
                array(
                    'Id'        => $factura->bon_intereses, // Id del tipo de IVA (5 para 21%)(ver tipos disponibles) 
                    'BaseImp'   => $factura->haber - ($factura->haber*((int)$porcentaje)/100 ), // Base imponible
                    'Importe'   => ($factura->haber*((int)$porcentaje)/100 )// Importe 
                )
            ),
        );
        //return $data;
        
        try {
            $afip = $this->_afip->ElectronicBilling->CreateVoucher($data);

            $factura = Factura::where('id',$id)->update(['num_factura'=>$num, 'cae'=>$afip['CAE'], 'caevencimiento'=>$afip['CAEFchVto']]);
            
            return Factura::where('id',$id)->first();

        } catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
        }

        
    }

}
