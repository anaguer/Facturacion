<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CreditCard;
use JWTAuth;
use Illuminate\Support\Facades\DB;

class creditCardController extends Controller
{
    public function Create(Request $request)
    {

    	$creditcard  = new Creditcard;

		$creditcard->debito = $request->debito;
		$creditcard->banco = $request->banco;
		$creditcard->nombretarjeta = $request->nombretarjeta;
		$creditcard->numerotarjeta = $request->numerotarjeta;
		$creditcard->vencimientotarjeta = $request->vencimientotarjeta;
		$creditcard->codigotarjeta = $request->codigotarjeta;
		$creditcard->codmes = $request->codmes;
		$creditcard->ano = $request->ano;
		$creditcard->personales_dni = $request->personales_dni;

		DB::insert('insert into credit_card (
            debito,
			banco,
			nombretarjeta,
			numerotarjeta,
			vencimientotarjeta,
			codigotarjeta,
			codmes,
			ano,
			personales_dni
        ) values (
            ?, ?, ?,?,?,?,?,?,?
        )', [
            $creditcard->debito,
			$creditcard->banco,
			$creditcard->nombretarjeta,
			$creditcard->numerotarjeta,
			$creditcard->vencimientotarjeta,
			$creditcard->codigotarjeta,
			$creditcard->codmes,
			$creditcard->ano,
			$creditcard->personales_dni
        ]);

		return $creditcard;
    }

    public function Edit(Request $request)
    {



    	$creditcard = [] ;





		if($request->banco)
			$creditcard['banco'] = $request->banco;

		if($request->nombretarjeta)
			$creditcard['nombretarjeta'] = $request->nombretarjeta;

		if($request->numerotarjeta)
			$creditcard['numerotarjeta'] = $request->numerotarjeta;

		if($request->vencimientotarjeta)
			$creditcard['vencimientotarjeta'] = $request->vencimientotarjeta;

		if($request->codigotarjeta)
			$creditcard['codigotarjeta'] = $request->codigotarjeta;

		Creditcard::where('id', $request->id)->update($creditcard);


		return Creditcard::where('id', $request->id)->first();
    }

    public function Delete(Request $request)
    {


        $creditcard = CreditCard::where('id',$request->id)->first();
        $creditcard->delete();
        return response()->json(['creditcard' => $creditcard], 200);
    }

    public function All(Request $request)
    {
    	if($request->page === "undefined"){
            if ($request->search === "undefined") {
                return Creditcard::all();
            }
            return Creditcard::where($request->search, $request->value)->get();
		}

		if($request->search === 'undefined')
			return Creditcard::paginate(5);


		return Creditcard::where($request->search, $request->value)->paginate(5);
    }

    public function Creditcard($id)
    {
    	return CreditCard::where('id',$id)->first();
    }

    public function GetClientCards(Request $request)
    {
    	return CreditCard::where('personales_dni',$request->dni)->get();
    }
}
