<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\User;
use App\Event;

class ApiEventsController extends Controller
{
    public function getHistory() {
      return Event::with('categories')->withCount('proposals')->get();
    }

    public function getUserEvents(Request $request) {
      try {
            $user = JWTAuth::parseToken()->toUser();
       	} catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
       	}
      return Event::with('categories')->withCount('proposals')->where('users_id', $user->id)->where('status', '<', 3)->get();
    }

    public function getEvent(Request $request) {
      return Event::with('categories')->withCount('proposals')->where('status', '<', 3)->find($request->id);
    }

    public function history(Request $request)
    {
     try {
          $user = JWTAuth::parseToken()->toUser();
      } catch (Exception $e) {
          return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
      }
      return Event::with('proposals.users.profile')->where('users_id', $user->id)->where('status', '>', 1)->get();
    }
    public function create(Request $request)
    {
    	try {
            $user = JWTAuth::parseToken()->toUser();
       	} catch (Exception $e) {
            return Response::json(['error' => $e->getMessage()], HttpResponse::HTTP_UNAUTHORIZED);
       	}
    	$event = new Event;
        $event->description = $request->description;
        $event->name = $request->name;
        $event->location = $request->location;
        $event->date = $request->date;
        $event->latitude = $request->latitude;
        $event->longitude = $request->longitude;
        $event->currencies_id = $request->currencies_id;
        $event->users_id = $user->id;

        $event->save();

        $event->categories()->attach($request->categories_id);

        return response()->json(['status' => "success"], 200);
    }

    public function updateEvent(Request $request, $id) {
      $event = Event::find($id);

      $event->description = $request->description;
      $event->name = $request->name;
      $event->date = $request->date;
      $event->latitude = $request->latitude;
      $event->longitude = $request->longitude;
      $event->currencies_id = $request->currencies_id;

      $event->save();

      $event->categories()->attach($request->categories_id);

      return response()->json(['status' => "success"], 200);
    }

    public function deleteEvent($id) {
      $event = Event::find($id);
      $event->delete();

      return response()->json(['status' => "success"], 200);
  }
}
