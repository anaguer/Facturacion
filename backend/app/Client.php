<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'personales';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    	'id',
		'dni',
		'nombre',
		'domicilio',
		'telefono',
		'email',
		'email2',
		'Barrio',
		'codigo_postal',
		'ciudad',
		'zona',
		'cod_barrio',
		'provincia',
		'servidor',
		'nodo',
		'ubicacion_nodo',
		'numero_servidor',
		'servidor_secundario',
		'tipo_equipo',
		'equipo',
		'ip',
		'ipequipo',
		'mac',
		'fecha_alta',
		'hora',
		'velocidad',
		'baja',
		'cuota',
		'bajadetalle',
		'baja_provisoria',
		'fecha_provisoria',
		'fecha_baja',
		'fecha_nac',
		'tolerancia',
		'fecha_instalacion',
		'chacra',
		'tolhuin',
		'instalar',
		'fecha_instalado',
		'obs_instala',
		'centro_ventas',
		'vencimiento_factura',
		'errorcorreo',
		'trabajo',
		'contrato',
		'orden',
		'condeuda',
		'baja_equipo',
		'ok',
		'barrapersonal',
		'adios',
		'comopaga',
		'red',
		'idap',
		'ap',
		'idservidor',
		'idantena',
		'tipofactura',
		'tipodoc',
		'codtipodoc',
		'condicioniva',
		'empresa',
		'debito',
		'banco',
		'tarjeta',
		'nombretarjeta',
		'numerotarjeta',
		'vencimientotarjeta',
		'codigotarjeta',
		'codmes',
		'ano',

	];

	public function users() {
	return $this->belongsToMany('App\User', 'personales_has_usuarios', 'personales_id', 'usuarios_id');
	}

    public $timestamps = false;

}
