<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturas_has_articulo_det extends Model
{
    protected $table = "facturas_has_articulo_det";
    protected $fillable = [
    	"facturas_id",
		"facturas_personales_id",
		"cantidad",
		"articulo_det_id"
    ];
    public $timestamps = false;

}
