<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $fillable = [
    	"id_ar",
		"codigo_art",
		"detalle_art",
		"unitario",
		"porcentaje",
    ];

    protected $table = "articulo_det";

    public $timestamps = false;

}
