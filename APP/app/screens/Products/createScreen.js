import React from 'react';
import {  View, StyleSheet,  TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import { Container, Header, Right, Left, Body, Content, Icon, Button, Title } from 'native-base';
import ERROR from '../../config/Error';
import Firebase from '../../config/firebase';
import Product from '../../services/product';


export default class EventosCreateScreen extends React.Component {

  constructor(props) {
    super(props);
  
    this.state = {
      token:this.props.screenProps.user.token,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      roles_idroles:1
      
    };
    if(this.props.navigation.state.params) {
      this.state = {...this.state, ...this.props.navigation.state.params.product}
    }
    
    this.submitValue = this.submitValue.bind(this)
  }

  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  submitValue(){
    
    if(!this.state.codigo_art){
      ERROR("Debe introducir un nombre","Error");
      return;
    }
    if(!this.state.detalle_art){
      ERROR("Debe introducir una descripcion","Error");
      return;
    }

    
    if(!this.state.unitario){
      ERROR("Debe introducir un dia","Error");
      return;
    } 

    if(!this.state.porcentaje){
      ERROR("Debe introducir un dia","Error");
      return;
    } 
    

    if(this.props.navigation.state.params) {
      this.setState({loading:true})
      Product.Update(this.state.token, this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateProducts`).set({value:true});
          ERROR("Producto actualizado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    } else {
      this.setState({loading:true})
      Product.Create(this.state.token ,this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateProducts`).set({value:true});
          ERROR("Producto creado con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    }
    
  }

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={()=>this.props.navigation.goBack()} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={{marginLeft:'-5%'}}>{this.props.navigation.state.params ? `Editando Producto ${this.props.navigation.state.params.product.codigo_art}` : 'Creando Producto'}</Title>
          </Body>
        </Header>    
        <Content>
          <ScrollView style={styles.container}>
            
            <Text style={styles.label}>Codigo</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.codigo_art}
              placeholderTextColor='#ccc'
              onChangeText={(text)=>this.onChangeValue('codigo_art', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>


            <Text style={styles.label}>Descripcion</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.detalle_art}
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('detalle_art', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            <Text style={styles.label}>Precio</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.unitario}
              keyboardType='numeric'
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('unitario', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>

            <Text style={styles.label}>Porcentaje</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              keyboardType='numeric'
              placeholderTextColor='#ccc'
              value={this.state.porcentaje}
              onChangeText={(text)=>this.onChangeValue('porcentaje', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>

            
          
            
            
              <TouchableOpacity 
                style={styles.button}
                onPress={this.submitValue}
                >
                {
                this.state.loading ?
                <ActivityIndicator color="#fff"/>
                :
                <Text style={styles.buttonText}>{this.props.navigation.state.params ? "Guardar producto" : "Registrar producto"}</Text>  
              }
              </TouchableOpacity>
            <View style={{backgroundColor:'transparent', height:50}} />
            

          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#888",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});


/*
<Select
          value={this.state.category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => this.setState({category:itemValue, categories_id: this.state.categories[itemIndex].id})}
          items={this.state.categories.map(data=>({label: data.name ,value: data.name ,key: data.id , color: '#ea1d75'}))}
        />
        
 */