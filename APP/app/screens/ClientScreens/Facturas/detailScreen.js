import React from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView, Text, Linking } from 'react-native';
import { Container, Header, Right, Left, Title, Body, Content, Button, Icon, Thumbnail } from 'native-base';
import ERROR from '../../../config/Error';
import Firebase from '../../../config/firebase';

export default class InvoiceDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          starCount: 0,
          proposal:[],
          isAcepted:false,
          proposalAcepted:{},
          isModal:false,
          isModalPay:false
        };


    }

    onStarRatingPress(rating) {
        this.setState({
          rating
        });
    }

    Estado(factura) {
        console.log(factura.estado, factura.pagado)
        if (factura.estado === '0' && factura.pagado === '0') 
          return 'No pagado';
        if (factura.pagado === '0') 
          return 'Pago en proceso';
        return 'Pagado' 
    }

    ColorEstado(factura) {
        if (factura.estado === '0' && factura.pagado === '0') 
            return 'red';
        if (factura.pagado === '0') 
            return 'yellow';
        return 'green';
    }



    render() {

        return (
            <Container>
                <Header style={{backgroundColor:'#2CA8FF'}}>
                <Left>
                    <Button onPress={()=>this.props.navigation.goBack()} transparent>
                    <Icon name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                  { this.props.navigation.state.params.factura.num_factura ?
                    <Title>{`Factura ${this.props.navigation.state.params.factura.num_factura}`}</Title>
                  : <Title>Factura</Title> }
                </Body>
                <Right>
                </Right>
                </Header>
                <Content>
                    <ScrollView>
                        <View style={styles.container}>
                            <View style={{alignItems:'center', marginTop:'5%'}}>
                                <Thumbnail style={{width:100, height:100, borderColor:'#888', borderWidth:1, padding:5}} source={require('../../../img/client.png')}/>
                            </View>
                            <View style={styles.evento}>
                              { this.props.navigation.state.params.factura.num_factura ?
                                <Text style={styles.eventoTitulo}>{`Numero: ${this.props.navigation.state.params.factura.num_factura}`}</Text>
                              : null }
                                <Text style={styles.eventoDescripcion}>{`Por : ${this.props.navigation.state.params.factura.debe}`}</Text>
                                  
                                    <Text style={{color:this.ColorEstado(this.props.navigation.state.params.factura)}}>{this.Estado(this.props.navigation.state.params.factura)}</Text>
                                 
                                <Text style={styles.eventoDescripcion}>{`Cantidad: 1`}</Text>
                                <Text style={styles.eventoDescripcion}>{`Descripcion: ${this.props.navigation.state.params.factura.descripcion}`}</Text>
                            </View>
                            <View style={{flexDirection:'row',justifyContent:'center'}}>
                                { this.props.navigation.state.params.factura.estado === '1' && this.props.navigation.state.params.factura.pagado === '1' ? <TouchableOpacity
                                    style={styles.buttonNext}
                                    onPress={()=>{Linking.openURL("https://facturacionblue.000webhostapp.com/pdf/generar?accion=ver&id="+ this.props.navigation.state.params.factura.cae)}}
                                    >
                                    <Text style={styles.buttonTextModal}>{"Ver Factura"}</Text>
                                </TouchableOpacity>
                                : null}
                                {this.props.navigation.state.params.factura.estado === '0' ?
                                <TouchableOpacity
                                    style={styles.buttonNext}
                                    onPress={()=>{this.props.navigation.navigate('PayInvoices', {selected:[this.props.navigation.state.params.factura], invoices:this.props.navigation.state.params.invoices})}}
                                    >
                                    <Text style={styles.buttonTextModal}>{"Pagar Factura"}</Text>
                                </TouchableOpacity>
                                :null
                                }
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
      }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,

    },

    evento: {
        paddingRight:15,
        borderRadius: 5,
        // shadowOpacity: 0.3,
        // shadowOffset: {width:0, height:0},
        // shadowColor: 'gray',
        // shadowRadius: 10,
        marginBottom:20,
        padding:20,
        backgroundColor: "#fff",
        // borderBottomColor: "gray",
        // borderBottomWidth: 2,
    },

    eventoTitulo : {
        fontSize: 17,
        marginBottom: 5,
        color: "#DEB749"
    },

    eventoDescripcion : {
        color: "#212121"
    },

    eventoFecha : {
        marginTop: 10,
        color: "#212121"
    },

    title : {
        fontSize: 18,
        marginBottom: 15,
        textAlign:'center'
    },

    proveedor : {
        padding: 20,
        borderBottomColor: "#ecf0f1",
        borderBottomWidth: 1,
        backgroundColor: "#fff",
    },

    proveedorNombre : {
        fontSize: 17,
        marginBottom: 5,
        fontWeight: 'normal',
        color: "#DEB749"
    },

    proveedorDescripcion : {

    },

    proveedorNombrePrecio: {
        flexDirection: 'row',
        justifyContent:'space-between',

    },

    proveedorPrecio : {
        fontSize: 18,
        color: "#ea1d75",
        fontWeight: 'bold'
    },

    proveedorAction : {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    buttonNext : {
        height: 40,
        backgroundColor:"#888",
        alignSelf: 'stretch',
        width:'35%',
        marginLeft:'10%',
        marginBottom:'5%'
    },

    buttonText : {
        paddingTop: 5,
        color: "#ea1d75",
        textAlign:'center',
        fontSize: 17,
    },

    buttonTextModal : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
        paddingVertical:10
    },
    buttonModal : {
        height: 50,
        backgroundColor:"#ea1d75",
        width:'80%',
        alignSelf: 'stretch',

        marginLeft:'10%',
        borderRadius: 3,
    },
    modal:{
        backgroundColor:'#eee',
        width:'70%',
        marginLeft:'15%',
        height:250,
        borderColor:'black',
        borderWidth:1,
        borderRadius:10,
        justifyContent:'center'

    },
    modalText:{
        color: "#000",
        textAlign:'center',
        fontSize: 17,
    },

    paypalLogo:{
        width:30,
        height:30
    }

});
