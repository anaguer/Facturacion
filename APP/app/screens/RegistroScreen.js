import React from 'react';
import { Button, Text, View, StyleSheet, Image, TextInput, TouchableOpacity, StatusBar, ScrollView, ActivityIndicator, Modal } from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import RadioButton from 'react-native-radio-button'
import User from '../services/user';
import ERROR from '../config/Error';
//import { Permissions, Notifications } from 'expo';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Select from 'react-native-picker-select';



export default class LoginScreen extends React.Component {
	static navigationOptions = {
		headerTintColor:"#fff",    
		headerStyle: {
				backgroundColor: '#000',
				borderColor: "#000",
		},
		header:null,
	}

	constructor(){
		super()
		this.state = {
			roles_id: 1,
			name:'',
			email:'',
			password:'',
			repetePassword:'',
			city:'',
			date:'',
			activity:false,
			isModal:false,
			payment:0,
			description:'',
			token:'a'
		};
		this.register = this.register.bind(this);
		//this.registerForPushNotificationsAsync();
		this.items = [
			"Aguascalientes",
 			"Baja California",
  			"Baja California Sur",
  			"Campeche",
  			"Chiapas",
  			"Chihuahua",
  			"Ciudad de México",
  			"Coahuila de Zaragoza",
  			"Colima",
  			"Durango",
  			"Estado de México",
  			"Guanajuato",
  			"Guerrero",
  			"Hidalgo",
  			"Jalisco",
  			"Michoacán de Ocampo",
  			"Morelos",
  			"Nayarit",
  			"Nuevo León",
  			"Oaxaca",
  			"Puebla",
  			"Querétaro",
  			"Quintana Roo",
  			"San Luis Potosí",
  			"Sinaloa",
  			"Sonora",
  			"Tabasco",
  			"Tamaulipas",
  			"Tlaxcala",
  			"Veracruz de Ignacio de la Llave",
  			"Yucatán",
  			"Zacatecas",
		]
	}

	handleIndexChange = (index) => {
		this.setState({
			roles_id: index+1,
		});
	}

	onChangeValue(name, value){
		this.setState({[name]:value})
	}

	async registerForPushNotificationsAsync() {
		const { status: existingStatus } = await Permissions.getAsync(
			Permissions.NOTIFICATIONS
		);
		let finalStatus = existingStatus;

		// only ask if permissions have not already been determined, because
		// iOS won't necessarily prompt the user a second time.
		if (existingStatus !== 'granted') {
			// Android remote notification permissions are granted during the app
			// install, so this will only ask on iOS
			const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
			finalStatus = status;
		}

		// Stop here if the user did not grant permissions
		if (finalStatus !== 'granted') {
			return;
		}

		// Get the token that uniquely identifies this device
		this.state.token = await Notifications.getExpoPushTokenAsync();

		console.log(this.state.token);
	}

	register(){
		//validacion
		let bandera = false;
		let errores = {errores:{errEmail:'', errPassword:''}}

		if (this.state.name === "") {
			bandera = true;
			ERROR("Debe introducir un nombre","Error");
			return;
		}

		if (this.state.email === "") {
			bandera = true;
			ERROR("Debe introducir un correo","Error");
			return;
		}

		if (this.state.password === "") {
			bandera = true;
			ERROR("Debe introducir una contraseña", "Error");
			return;
		}

		if (this.state.repetePassword === "") {
			bandera = true;
			ERROR("Repita la contraseña", "Error");
			return;
		} else if (this.state.password !== this.state.repetePassword) {
			bandera = true;
			ERROR("Contraseñas no coinciden", "Error");
			return;
		}

		if (this.state.city === "") {
			bandera = true;
			ERROR("Debe introducir una ciudad", "Error");
			return;
		}

		if(this.state.dia === ""){
			bandera = true;
			ERROR("Debe introducir un dia","Error");
			return;
		} 
		if(this.state.mes === "" ){
			bandera = true;
			ERROR("Debe introducir un mes","Error");
			return;
		} 
		if( this.state.anio === ""){
			bandera = true;
			ERROR("Debe introducir un año","Error");
			return;
		} 

		if (!bandera) {
			this.state.date = this.state.anio + '/' + this.state.mes + '/' + this.state.dia
			this.setState({activity:true})
			User.register(this.state)
			.then(
				(dataToken) => {
					
					if(!dataToken.error){
						User.me(dataToken.access_token)
							.then(dataUser=>{
								console.log(dataUser)
								dataUser.user.password = this.state.password;
								this.props.screenProps.logIn({token:dataToken.access_token, user:dataUser.user, profile:dataUser.profile});
								this.setState({activity:false})
							})
					}else {
						ERROR("Credenciales inválidas", "Error");
						this.setState({activity:false})
						return;
					}
				}
			).catch(error => {
				ERROR("Correo esta ya registrado", "Error");
						this.setState({activity:false})
			})
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<StatusBar barStyle = "light-content" backgroundColor="#000" />
				<ScrollView contentContainerstyle={[styles.container, {width:'100%',alignItems: "center", justifyContent: "center",}]}>
					<View style={{justifyContent:'center', alignItems:'center'}}>	
						<Image
							style={styles.logo}
							source={require('../img/logo.png')}
						/> 
					</View>
					
					<TextInput 
						style={styles.input}
						placeholder="Nombres"
						placeholderTextColor='rgba(255,255,255, 0.7)'
						underlineColorAndroid={'rgba(0,0,0,0)'}
						onChangeText={(text)=>this.onChangeValue('name', text)}
						>
					</TextInput>

					<TextInput 
						style={styles.input}
						placeholder="Correo Electrónico"
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						onChangeText={(text)=>this.onChangeValue('email', text)}
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>

					 <TextInput 
						style={styles.input}
						placeholder="Contraseña"
						secureTextEntry
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						onChangeText={(text)=>this.onChangeValue('password', text)}
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>

					<TextInput 
						style={styles.input}
						placeholder="Confirmar Contraseña"
						secureTextEntry
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						onChangeText={(text)=>this.onChangeValue('repetePassword', text)}
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>

					

					<View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
						<Select
		                    placeholder={{
		                        label: 'seleciona departamento...',
		                        value: null,
		                    }}
		                    items={this.items.map(data=>({label:data, value:data, color:'black'}))}
		                    onValueChange={(value) => this.onChangeValue('city', value)}
		                    value={this.state.city}
		                    
		                />
					</View>
					


				 <View style={{'flexDirection':'row'}}>  
					<TextInput 
						style={[styles.input, {'width':'32%'}]}
						placeholder="DD"
						keyboardType='numeric'
						maxLength={2}
						onChangeText={(text)=>this.onChangeValue('dia', text)}
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>
					<TextInput 
						style={[styles.input, {'marginLeft':'2%', 'width':'32%'}]}
						placeholder="MM"
						keyboardType='numeric'
						maxLength={2}
						onChangeText={(text)=>this.onChangeValue('mes', text)}
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>
					<TextInput 
						style={[styles.input, {'marginLeft':'2%', 'width':'32%'}]}
						placeholder="AAAA"
						keyboardType='numeric'
						maxLength={4}
						onChangeText={(text)=>this.onChangeValue('anio', text)}
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>
				</View>

				{this.state.roles_id === 1 ?
				<View>
					<Text style={[styles.label, {fontWeight:'bold'}]}>Destacar publicaciones</Text>
					<View style={{'flexDirection':'row'}}>
						<RadioButton
							animation={'bounceIn'}
							innerColor={'#ea1d75'}
							outerColor={'#ea1d75'}
							isSelected={this.state.payment}
							onPress={() => this.setState({isModal:!this.state.payment, payment:Number(!this.state.payment)})}
						/>
						<Text style={[styles.label,{marginLeft:5}]} >
						 {"Suscripción Premium"}
						</Text>
					</View>
				</View>
				: null}

					<SegmentedControlTab
							values={['Proveedor', 'Cliente']}
							selectedIndex={this.state.roles_id-1}
							onTabPress={this.handleIndexChange}
							tabStyle={{borderColor:'transparent',marginTop:20 , marginBottom:20, height: 50}}
							tabTextStyle={{color: "#666"}}
							activeTabStyle={{backgroundColor:"#feda73"}}
							activeTabTextStyle={{color: "#000"}}
					/>

					<TouchableOpacity 
						style={styles.button}
						onPress={this.register}
						>
						{!this.state.activity ?
							<Text style={styles.buttonText}>Registrarse</Text>  
							:
							<ActivityIndicator size="small" color="#fff" />
						}
						
					</TouchableOpacity>

					<TouchableOpacity 
						style={styles.link}
						onPress={() => this.props.navigation.navigate('Login')}
						>
						<Text style={styles.buttonText}>Iniciar Sesión</Text>  
					</TouchableOpacity> 

					<Modal
						animationType="slide"
						transparent={true}
						visible={this.state.isModal}
						
						onRequestClose={() => {
							alert('Modal has been closed.');
						}}>
						<View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%'}}>
							<View style={styles.modal}>
								<TouchableOpacity 
										style={{position:'absolute', top:5,left:5}}
										onPress={()=>this.setState({isModal:false, payment:0})}
										>
									<MaterialIcons size={30} name='clear' />
								</TouchableOpacity>
								<View style={{flex:0.4}}>
									<Text style={[styles.modalText, {marginTop:'10%'}]}>{"Pagar via paypal"}</Text> 
								</View>
								<View style={{flex:0.3, justifyContent:'center', alignItems:'center'}}>
									<Image
										style={styles.paypalLogo}
										source={require('../img/PayPal-simbolo.png')}
									/> 
								</View>
								<View style={{flex:0.3}}>
									<Text style={styles.modalText}>{"10 USD mensual"}</Text> 
								</View>
								<View style={{flex:0.05}}>
									<TouchableOpacity 
										style={styles.buttonModal}
										onPress={()=>this.setState({isModal:false, payment:1})}
										>
										
										<Text style={styles.buttonText}>{"Pagar"}</Text>  
										 
									</TouchableOpacity>
								</View>
									
							</View>
						</View>
					</Modal> 
				</ScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#000",
		padding: 20,
	},

	input: {
		padding: 10,
		backgroundColor:'rgba(255, 255, 255, 0.3)',
		height: 50,
		alignSelf: 'stretch',
		color:"#fff",
		fontSize: 17,
		marginBottom: 20,
		borderRadius: 3,
		width:'100%'
	},

	label : {
			fontSize: 17,
			color: "gray",
			marginBottom: 3
		},

	logo : {
		width: 100,
		resizeMode: 'contain',
		height: 100,
		marginBottom:20,
		
	},

	button : {
		height: 50,
		backgroundColor:"#ea1d75",
		// backgroundColor:"#feda73",
		alignSelf: 'stretch',
		paddingVertical: 15,
		borderRadius: 3,
	},
	buttonModal : {
		height: 50,
		backgroundColor:"#ea1d75",
		width:'80%',
		alignSelf: 'stretch',
		paddingVertical: 15,
		marginLeft:'10%',
		borderRadius: 3,
	},
	buttonText : {
		color: "#fff",
		textAlign:'center',
		fontSize: 17,
	},

	link : {
		marginTop: 30,
	},

	modal:{
		backgroundColor:'#eee',
		width:'60%',
		marginLeft:'20%',
		height:'40%',
		marginTop:'30%',
		borderColor:'black',
		borderWidth:1,
		borderRadius:10,
		justifyContent:'center'

	},
	modalText:{
		color: "#000",
		textAlign:'center',
		fontSize: 17,
	},

	paypalLogo:{
		width:30,
		height:30
	}




});


