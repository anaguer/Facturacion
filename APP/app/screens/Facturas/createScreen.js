import React from 'react';
import {  View, StyleSheet,  TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import { Container, Header, Right, Left, Body, Content, Icon, Button, Title } from 'native-base';
import ERROR from '../../config/Error';
import Firebase from '../../config/firebase';
import client from '../../services/client';
import product from '../../services/product';
import factura from '../../services/factura';
import Select from 'react-native-picker-select';
import { Table, Row, Rows } from 'react-native-table-component';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';

export default class EventosCreateScreen extends React.Component {

  constructor(props) {
    super(props);
  
    this.state = {
      token:this.props.screenProps.user.token,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      roles_idroles:1,
      estado:'No pagado',
      clients:[],
      products:[],
      showAddProduct:false,
      head:['num', 'codigo', 'cantidad', 'importe'],
      productsSelect:[],
      importe_pagado:0,
      mora:0,
      bon_intereses:0,
      haber:0,
      pagado:0,
      tipo:1
      
    };
    if(this.props.navigation.state.params) {
      
      this.state = {...this.state, ...this.props.navigation.state.params.factura}
    }
    this.addProduct = this.addProduct.bind(this);    
    this.submitValue = this.submitValue.bind(this)
  }

  componentWillMount(){
    product.All(this.state.token)
      .then(products=>this.setState({products}))
    client.All(this.state.token)
      .then(clients=>this.setState({clients}))
  }

  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  submitValue(){
    
    if (!this.state.num_factura){
      ERROR("Debe introducir un numero de factura","Error");
      return;
    }
    if (!this.state.descripcion){
      ERROR("Debe introducir una descripcion","Error");
      return;
    }
    if (!this.state.vencimiento){
      ERROR("Debe introducir una fecha","Error");
      return;
    } 

    
    if (!this.state.personales_id) {
      ERROR("Debe introducir un cliente ", 'Error');
      return;
    }

    
    if (!this.state.tipo && this.state.tipo !== 0) {
      ERROR("Debe introducir el tipo de transaccion", 'Error');
      return;
    }

    

    if(this.props.navigation.state.params) {
      this.setState({loading:true})
      factura.Update(this.state.token, this.state)
        .then(data=>{
          console.log(data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateFacturas`).set({value:true});
          ERROR("Factura actualizada con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    } else {
      this.setState({loading:true})
      factura.Create(this.state.token ,this.state)
        .then(data=>{
          console.log("creada",data)
          Firebase.database().ref(`update/${this.props.screenProps.user.user.id}/updateFacturas`).set({value:true});
          ERROR("Factura creada con éxito","exito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
    }
    
  }

  addProduct(){
    if (!this.state.cantidad || Number(this.state.cantidad)===0) {
      ERROR("Debe introducir el tipo de cantidad", 'Error');
      return;
    }
    this.state.productsSelect.push({...this.state.products[this.state.indexProduct], cantidad:this.state.cantidad})
    let importe_pagado = 0;
    for (let i = 0; i < this.state.productsSelect.length; i++) {
      importe_pagado += this.state.productsSelect[i].cantidad*this.state.productsSelect[i].unitario;
      
    }
    this.setState({cantidad:null, indexProduct:null, showAddProduct:false, importe_pagado})
  }

  

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#2CA8FF'}}>
          <Left>
            <Button onPress={()=>this.props.navigation.goBack()} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={{marginLeft:'-5%'}}>{this.props.navigation.state.params ? `Editando factura ${this.props.navigation.state.params.client.nombre}` : 'Creando factura'}</Title>
          </Body>
        </Header>    
        <Content>
          <ScrollView style={styles.container}>
            
            <Text style={styles.label}>Numero de factura</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.num_factura}
              placeholderTextColor='#ccc'
              onChangeText={(text)=>this.onChangeValue('num_factura', text)}
              underlineColorAndroid={'#2CA8FF'}
              >
            </TextInput>


            <Text style={styles.label}>Descripcion</Text>
            <TextInput 
              style={styles.input}
              placeholder=""
              value={this.state.descripcion}
              placeholderTextColor= '#ccc'
              onChangeText={(text)=>this.onChangeValue('descripcion', text)}
              underlineColorAndroid={'#2CA8FF'}>
              
            </TextInput>
            <Text style={styles.label}>Tipo de pago</Text>
            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
              <Select
                  placeholder={{
                      label: 'seleciona un el tipo de pago...',
                      value: null,
                  }}
                  items={[{label:'efectivo', value:0, color:'black'},{label:'Tarjeta de credito', value:2, color:'black'},{label:'Por cuotas', value:3, color:'black'}]}
                  onValueChange={(value) => this.onChangeValue('tipo', value)}
                  value={this.state.tipo}
                  
              />
            </View>

            <Text style={styles.label}>Fecha</Text>
            <TouchableOpacity onPress={(text)=>this.onChangeValue('isDateTimePickerVisible', true)}>
              <TextInput 
                style={styles.input}
                placeholder=""
                value={this.state.vencimiento}
                keyboardType='numeric'
                placeholderTextColor= '#ccc'
                editable={false}
                
                underlineColorAndroid={'#2CA8FF'}>
                
              </TextInput>
            </TouchableOpacity>

            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={(text)=>this.onChangeValue('vencimiento', Moment(text.toString()).format('YYYY-MM-DD'))}
              onCancel={(text)=>this.onChangeValue('isDateTimePickerVisible', false)}
            />

            <Text style={styles.label}>Clientes</Text>
            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
              <Select
                  placeholder={{
                      label: 'seleciona un cliente...',
                      value: null,
                  }}
                  items={this.state.clients.map(data=>({label:data.nombre, value:data.id, color:'black'}))}
                  onValueChange={(value) => this.onChangeValue('personales_id', value)}
                  value={this.state.personales_id}
                  
              />
            </View>

            <Text style={styles.label}>Productos</Text>
            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:20}}>
              <Select
                  placeholder={{
                      label: 'seleciona un producto...',
                      value: null,
                  }}
                  items={this.state.products.map((data,i)=>({label:data.codigo_art, value:i, color:'black'}))}
                  onValueChange={(indexProduct) => this.setState({showAddProduct:true, indexProduct})}
                  value={this.state.indexProduct}
                  
              />
            </View>

            {
              this.state.showAddProduct &&  this.state.products[this.state.indexProduct] ? <View style={{borderRadius:10, borderColor:'#2CA8FF', borderWidth:2}}>
                <Text style={[styles.label,{textAlign:'center', fontWeight:'bold'}]}>{'Detalle Producto'}</Text>
                
                <View style={{flexDirection:'row', flex:1}}>
                  <View style={{flex:0.5, padding:5}}>
                    <Text style={styles.label}>Codigo</Text>
                    <TextInput 
                      style={styles.input}
                      placeholder=""
                      value={this.state.products[this.state.indexProduct].codigo_art}
                      placeholderTextColor='#ccc'
                      editable={false}
                      underlineColorAndroid={'#2CA8FF'}
                      >
                    </TextInput>
                  </View>
                  <View style={{flex:0.5, padding:5}}>
                    <Text style={styles.label}>Porcentaje</Text>
                    <TextInput 
                      style={styles.input}
                      placeholder=""
                      value={this.state.products[this.state.indexProduct].porcentaje}
                      placeholderTextColor= '#ccc'
                      editable={false}
                      keyboardType='numeric'
                      underlineColorAndroid={'#2CA8FF'}>
                      
                    </TextInput>
                  </View>
                </View>
                <View style={{flexDirection:'row', flex:1}}>
                  <View style={{flex:0.5, padding:5}}>
                    <Text style={styles.label}>Cantidad</Text>
                    <TextInput 
                      style={styles.input}
                      placeholder=""
                      value={this.state.cantidad}
                      keyboardType='numeric'
                      placeholderTextColor= '#ccc'
                      onChangeText={(text)=>this.onChangeValue('cantidad', text)}
                      underlineColorAndroid={'#2CA8FF'}>
                      
                    </TextInput>
                  </View>
                  <View style={{flex:0.5, padding:10}}>
                    <TouchableOpacity 
                      style={[styles.button, {marginTop:'15%'}]}
                      onPress={this.addProduct}
                      >
                        <Text style={styles.buttonText}>{'Agregar'}</Text>  
                    </TouchableOpacity>
                  </View>
                </View>

                
              </View>
              :null
            }
            <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff', marginTop:'5%'}}>
              <Row data={this.state.head} style={styles.head} textStyle={styles.text}/>
              <Rows data={this.state.productsSelect.map((product,i)=>([i+1,product.codigo_art,product.cantidad,product.unitario*product.cantidad ]))} textStyle={styles.text}/>
              <Row data={['total','','',this.state.importe_pagado]} style={styles.head} textStyle={styles.text}/>
            </Table>

            <TouchableOpacity 
              style={styles.button}
              onPress={this.submitValue}
              >
              {
              this.state.loading ?
              <ActivityIndicator color="#fff"/>
              :
              <Text style={styles.buttonText}>{this.props.navigation.state.params ? "Guardar factura" : "Registrar factura"}</Text>  
            }
            </TouchableOpacity>
            
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },
    head: { height: 40, backgroundColor: '#f1f8ff' },
    text: { margin: 6, fontSize:10   },  

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#888",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});


/*
<Select
          value={this.state.category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => this.setState({category:itemValue, categories_id: this.state.categories[itemIndex].id})}
          items={this.state.categories.map(data=>({label: data.name ,value: data.name ,key: data.id , color: '#ea1d75'}))}
        />
        
 */