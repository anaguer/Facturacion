import React from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import StarRating from 'react-native-star-rating';
import Categories from '../services/categories';

export default class ProveedorScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      events:[]
    };
    Categories.profileEvent(this.props.navigation.state.params.categories_id)
            .then(data=>this.setState({events:data[0].profiles ? data[0].profiles : []}))

  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  render() {
    console.log(this.state.events)
    return (
      <ScrollView style={styles.contenedor}>
      {this.state.events.map(data=>(
          <View style={styles.proveedor}>
            <Text style={styles.proveedorNombre}>{data.name}</Text>
            <Text style={styles.proveedorDescripcion}>{data.description}</Text>
            <StarRating
              disabled={true}
              maxStars={5}
              rating={data.rating/data.finishWork}
              selectedStar={(rating) => this.onStarRatingPress(rating)}
              fullStarColor={'#f1c40f'}
              starSize={20}
              containerStyle={{width: 150, marginTop:10}}
            />
          </View>
        ))}

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  proveedor : {
    padding: 20,
    borderBottomColor: "#ecf0f1",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
  },

  proveedorNombre : {
    fontSize: 17,
    marginBottom: 5,
    fontWeight: 'normal',
    color: "#DEB749"
  },

  proveedorDescripcion : {

  }

});


