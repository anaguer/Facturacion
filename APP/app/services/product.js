import Base from './base';



class Product extends Base {

	All = (token, page) => {
		return this.get('product',{token, page});
	}

	Create = (token , data) => {
		return this.post('product',{token, ...data});
	}
	Get = ( token, id ) => {
		return this.get('product/' + id, {token});
	}

	Delete = (token, id) => {
		return this.delete('product', {token, id});
	}

	Update = (token, data) => {
		return this.put('product', {...data, token});
	}
}

export default new Product;