import React from 'react';
import { Text, View, StyleSheet, ScrollView, TextInput, TouchableOpacity, Image, Modal } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import StarRating from 'react-native-star-rating';
import RadioButton from 'react-native-radio-button'
import Categories from '../services/categories';
import User from '../services/user';
import ERROR from '../config/Error';
import Slider from 'react-native-image-slider';
import expo from 'expo';
import ImageSlider from '../services/imagenSlider';
import Select from 'react-native-picker-select';

export default class PerfilScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      userId: this.props.screenProps.user.user.id,
      text:'',
      categories:[],
      user:{},
      profile:{},
      categoriasSelected:{},
      profileToSend:{},
      Images:[
      ],
      isModal:false,
      indexImagenEdit:0,
    };

    Categories.getCategories()
      .then(data=>this.setState({categories:data ? data : []}))
    User.me(this.props.screenProps.user.token)
      .then(dataUser=>{
        this.setState({user:dataUser.user,profile:dataUser.profile});
      })
    ImageSlider.ALL(this.props.screenProps.user.user.id)
      .then(data=>{
          this.setState({Images:data ? data : []});
      })

    this.submitValue = this.submitValue.bind(this);
    this.addImage = this.addImage.bind(this);
    this.DeleteImage = this.DeleteImage.bind(this);
    this.editImage = this.editImage.bind(this);
    
  }
  submitValue(){
    User.updateUser(this.state.profileToSend).then((data)=>{
      ERROR("Perfil Actualizado","Exito");
    });
  }

  async addImage() {
    
    expo.ImagePicker.launchImageLibraryAsync({
        mediaTypes:'Images',
        allowsEditing:true  
    })
        .then(result => {
            if(!result.cancelled){
                this.setState({loadingAddImage:true})
                Image.Create(this.props.screenProps.user.user.id, { uri: result.uri })
                    .then(data=>{
                        this.state.Images.push(data.image)
                        this.setState({Images:this.state.Images, loadingAddImage:false})
                        ERROR("Imagen Agregada","Exito");
                    })
            }
        })

  }

  onChangeValue(name, value) {
    this.setState({profileToSend:{
      ...this.state.profile,
      [name] : value
    }})
  }

  async editImage() {
    const result = await  expo.ImagePicker.launchImageLibraryAsync({
        mediaTypes:'Images',
        allowsEditing:true
        
    })
    if(!result.cancelled){
        this.setState({loadingEditImage:true})
        Image.Update(this.state.Images[this.state.indexImagenEdit].id, this.props.screenProps.user.user.id)
            .then(data=>{
                this.state.Images[this.state.indexImagenEdit] = data.image
                this.setState({Images:this.state.Images, isModal:false, loadingEditImage:false});
                ERROR("Imagen Editada","Exito");
            })
    }
  }

  DeleteImage() {
    this.setState({loadingDeleteImage:true})
    Image.Update(this.state.Images[this.state.indexImagenEdit].id, { uri: result.uri })
    .then(data=>{
        this.setState({loadingDeleteImage:false,Images:this.state.Images.filter((data, i)=> i !== this.state.indexImagenEdit )})   
        ERROR("Imagen eliminada","Exito");
    }) 
  }

  render() {

    return (
        <ScrollView style={styles.container}>
        
            <View style={{'flexDirection':'row'}}>
                <View style={{'flexDirection':'column', 'flex':0.6}}>
                    <Text style={[styles.label,{marginLeft:5}]} >
                        {"Nombre"}
                    </Text>
                    <TextInput 
                        style={styles.input}
                        placeholder={this.state.profile.name}
                        placeholderTextColor='#ccc'
                        underlineColorAndroid={'rgba(0,0,0,0)'}
                        onChangeText={(text)=>this.onChangeValue('name', text)}
                        >
                    </TextInput>
                </View>
                <View style={{'flexDirection':'column', 'flex':0.4, marginLeft:'2%'}}>
                    <Text style={styles.label} >
                        {"Calificación"}
                    </Text>
                    <View style={styles.proveedorAction}>
                        <StarRating
                            disabled={true}
                            maxStars={5}
                            rating={this.state.profile.rating/this.state.profile.finishWork}
                            fullStarColor={'#f1c40f'}
                            starSize={12}
                            containerStyle={{width: 80, marginTop:10}}
                        />
                    </View>

                </View>
            </View>


        <Text style={styles.label}>Descripción</Text>
        <TextInput 
          style={styles.textarea}
          placeholder={this.state.profile.description}
          placeholderTextColor= '#ccc'
          multiline={true}
          numberOfLines={10}
          onChangeText={(text)=>this.onChangeValue('description', text)}
          underlineColorAndroid={'rgba(0,0,0,0)'}>
          
        </TextInput>

        <Text style={styles.label}>Pagina web</Text>
        <TextInput 
          style={styles.input}
          placeholder={this.state.profile.website}
          placeholderTextColor='#ccc'
          onChangeText={(text)=>this.onChangeValue('website', text)}
          underlineColorAndroid={'rgba(0,0,0,0)'}
          >
        </TextInput>

        {this.state.categories.map((data, i)=>(
            i%2 === 0 ?
            <View style={{'flex':1, 'flexDirection':'row'}}>
                <View style={{'flex':0.5, 'flexDirection':'row', marginLeft:'2%', 'marginTop':'2%', 'alignItems':'center'}}>
                    <RadioButton
                        animation={'bounceIn'}
                        innerColor={'#ea1d75'}
                        outerColor={'#ea1d75'}
                        isSelected={this.state.categoriasSelected[i]}
                        onPress={() => this.setState({categoriasSelected:{...this.state.categoriasSelected, [i]:!this.state.categoriasSelected[i]}})}
                    />
                   <Text style={[styles.label, {marginLeft:'2%'}]}>{data.name}</Text>
               </View>
               { this.state.categories[i+1] ?
                    <View style={{'flex':0.5, 'flexDirection':'row', marginLeft:'2%', 'marginTop':'2%', 'alignItems':'center'}}>
                       <RadioButton
                            animation={'bounceIn'}
                            innerColor={'#ea1d75'}
                            outerColor={'#ea1d75'}
                            isSelected={this.state.categoriasSelected[i+1]}
                            onPress={() => this.setState({categoriasSelected:{...this.state.categoriasSelected, [i+1]:!this.state.categoriasSelected[i+1]}})}
                        />
                       <Text style={[styles.label, {marginLeft:'2%'}]}>{this.state.categories[i+1].name}</Text>
                   </View>
                :null}
            </View>
            : null
             ))}
        
        {this.state.categories.length %2 !== 0 ?
            <View style={{'flexDirection':'row', marginLeft:'2%', 'marginTop':'2%', 'alignItems':'center'}}>
                <RadioButton
                    animation={'bounceIn'}
                    innerColor={'#ea1d75'}
                    outerColor={'#ea1d75'}
                    isSelected={this.state.categoriasSelected[this.state.categories.length - 1]}
                    onPress={() => this.setState({categoriasSelected:{...this.state.categoriasSelected, [this.state.categories.length - 1]:!this.state.categoriasSelected[this.state.categories.length - 1]}})}
                />
               <Text style={styles.label}>{this.state.categories[this.state.categories.length -1].name}</Text>
            </View>
            : null
        }
        <View style={{height:'20%', position:'relative', marginTop:'5%'}}>
            <Slider
                loopBothSides
                autoPlayWithInterval={3000}
                images={this.state.Images.map(data=>'https://hivata.herokuapp.com/api/retrieve-image/'+data.path)}
                onPress={({ index }) => this.setState({indexImagenEdit:index, isModal:true})}
                customSlide={({ index, item, style, width }) => {
                    
                // It's important to put style here because it's got offset inside
                return(
                    <View key={index} style={[style, styles.customSlide]}>
                        <Image ImageResizeMode='cover' source={{ uri: item }} style={[styles.customImage, {width, height:'100%'}]} />
                    </View>
                    );
                }}
                customButtons={(position, move) => (
                    <View style={styles.buttons}>
                        {
                            this.state.Images.map((image, index) => {
                                return (
                                    <TouchableOpacity
                                        key={index}
                                        underlayColor="#ccc"
                                        onPress={() => move(index)}
                                        style={styles.buttonSlider}
                                    >
                                    <Text style={position === index ? styles.buttonSelected : styles.contentText}>
                                        {index + 1}
                                    </Text>
                                    </TouchableOpacity>
                                );
                            })
                        }
                    </View>
                )}
            
            />
            {this.state.Images.length > 0 && <TouchableOpacity 
                style={[styles.buttonPlusSlider,this.state.Images.length === 5 ? {paddingVertical:5} : {right:37, paddingVertical:5}]}
                onPress={()=>this.setState({isModal:true})}>
                {
                    <Ionicons style={styles.buttonText} name='md-create'/>   
                }
            </TouchableOpacity>}
            {this.state.Images.length < 5 && <TouchableOpacity 
                style={styles.buttonPlusSlider}
                onPress={this.addImage}>
                {
                    
                    this.state.loadingAddImage ?
                    <ActivityIndicator color="#fff"/>
                    :
                    <Text style={styles.buttonText}>+</Text>    
                    
                    
                }
            </TouchableOpacity>}
            <Modal
                animationType="slide"
                transparent={true}
                
                visible={this.state.isModal}
                
                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%', justifyContent:'center'}}>
                    
                    <View style={styles.modal}>
                        <TouchableOpacity 
                                style={{position:'absolute', top:5,left:5}}
                                onPress={()=>this.setState({isModal:false})}
                                >
                            <MaterialIcons size={30} name='clear' />
                        </TouchableOpacity>
                        <View style={{flex:0.6, alignItems:'center'}}>
                            <Text style={[styles.modalText]}>{"Edita tu imagenes"}</Text> 
                            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:15, width:'80%'}}>
                                <Select
                                    placeholder={{
                                        label: 'seleciona una imagen...',
                                        value: -1,
                                    }}
                                    items={this.state.Images.map((data, i)=>({label:`Imagen ${i}`, value:i, color:'black'}))}
                                    onValueChange={(value) => this.setState({indexImagenEdit:value})}
                                    value={this.state.city}
                                />
                            </View>
                            <Image
                                style={[styles.paypalLogo,{marginTop:'7%'}]}
                                source={{ uri: this.state.Images[this.state.indexImagenEdit] }}
                            /> 
                        
                         
                        </View>
                        <View style={{flex:0.1, flexDirection:'row'}}>
                            <TouchableOpacity 
                                style={styles.buttonModal}
                                onPress={()=>this.DeleteImage()}
                                >
                                {
                                    this.state.loadingDeleteImage ?
                                    <ActivityIndicator color="#fff"/>
                                    :
                                    <Text style={styles.buttonTextModal}>{"Eliminar"}</Text>  
                                }
                                  
                                    
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={styles.buttonModal}
                                onPress={()=>this.editImage()}
                                >
                                {
                                    this.state.loadingEditImage ?
                                    <ActivityIndicator color="#fff"/>
                                    :
                                    <Text style={styles.buttonTextModal}>{"Cambiar"}</Text>  
                                }

                                    
                            </TouchableOpacity>
                        </View>
                            
                    </View>
                    
                </View>
            </Modal>
        </View>
        
        
        <Text style={[styles.label,{marginLeft:5, marginTop:'5%'}]} >
            {"Persona de contacto"}
        </Text>
        <TextInput 
            style={styles.input}
            placeholder={this.state.profile.contactName}
            placeholderTextColor='#ccc'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('contactName', text)}
            >
        </TextInput>

        <Text style={[styles.label,{marginLeft:5}]} >
            {"Email"}
        </Text>
        <TextInput 
            style={styles.input}
            placeholder={this.state.profile.contactEmail}
            placeholderTextColor='#ccc'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('contactEmail', text)}
            >
        </TextInput>

        <Text style={[styles.label,{marginLeft:5}]} >
            {"Telefono"}
        </Text>
        <TextInput 
            style={styles.input}
            placeholder={this.state.profile.phone}
            placeholderTextColor='#ccc'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('phone', text)}
            >
        </TextInput>

        <Text style={[styles.label,{marginLeft:5}]} >
            {"Celular"}
        </Text>
        <TextInput 
            style={styles.input}
            placeholder={this.state.profile.cellPhone}
            placeholderTextColor='#ccc'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('cellPhone', text)}
            >
        </TextInput>
        
        <TouchableOpacity 
          style={styles.button}
          onPress={this.submitValue}>
          {
            this.state.loading ?
            <ActivityIndicator color="#fff"/>
            :
            <Text style={styles.buttonText}>Actualizar</Text>  
          }
        </TouchableOpacity>

          <View style={{backgroundColor:'transparent', height:50}} />
          
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
     container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 15,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    contentText: { color: '#fff' },

    button : {
        height: 50,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonPlusSlider : {
        height: 30,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 3,
        width:30,
        borderRadius:250,
        position:'absolute',
        top: 5,
        right: 5
    },
    


    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
    proveedorAction : {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    customSlide: {
        backgroundColor: '#eee',
        alignItems: 'center',
        justifyContent: 'center',
       
        
    },
    customImage: {
        width: 100,
        height: 100,
    },
    buttons: {
        zIndex: 1,
        height: 15,
        marginTop: -25,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    buttonSlider: {
        margin: 3,
        width: 15,
        height: 15,
        opacity: 0.9,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTextModal : {
        color: "#fff",
        textAlign:'center',
        fontSize: 14,
        paddingVertical:13
    },
    buttonModal : {
        height: 50,
        backgroundColor:"#ea1d75",
        width:'30%',
        alignSelf: 'stretch',
        marginLeft:'12.5%',
        borderRadius: 5,
    },
    modal:{
        backgroundColor:'#eee',
        width:'70%',
        marginLeft:'15%',
        height:300,
        borderColor:'black',
        borderWidth:1,
        borderRadius:10,
        justifyContent:'center'

    },
    modalText:{
        color: "#000",
        textAlign:'center',
        fontSize: 17,
    },

    paypalLogo:{
        width:100,
        height:60
    }

});


