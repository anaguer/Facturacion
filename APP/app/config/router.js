import React from "react";
import { Platform } from "react-native";

import {
  createStackNavigator,
  SafeAreaView,
  createDrawerNavigator
} from "react-navigation";

import LoginScreen from "../screens/LoginScreen";
import RegistroScreen from "../screens/RegistroScreen";
import RecuperarScreen from "../screens/RecuperarScreen";

import SideBar from '../components/SideBar';
import ClientsSidebar from '../components/ClientsSidebar';

import usersScreen from "../screens/Users/screen";
import UsersCreateScreen from "../screens/Users/createScreen";
import UsersDetailScreen from "../screens/Users/detailScreen";

import ProductsScreen from "../screens/Products/screen";
import ProductsCreateScreen from "../screens/Products/createScreen";
import ProductsDetailScreen from "../screens/Products/detailScreen";

import ClientsScreen from "../screens/Clients/screen";
import ClientsCreateScreen from "../screens/Clients/createScreen";
import ClientsDetailScreen from "../screens/Clients/detailScreen";

import FacturasScreen from "../screens/Facturas/screen";
import FacturasCreateScreen from "../screens/Facturas/createScreen";
import FacturasDetailScreen from "../screens/Facturas/detailScreen";

import RecibosScreen from "../screens/Recibos/screen";
import RecibosCreateScreen from "../screens/Recibos/createScreen";
import RecibosDetailScreen from "../screens/Recibos/detailScreen";

import ReclamosScreen from "../screens/Reclamos/screen";
import ReclamosCreateScreen from "../screens/Reclamos/createScreen";
import ReclamosDetailScreen from "../screens/Reclamos/detailScreen";

import ClientInvoices from "../screens/ClientScreens/Facturas/screen";
import InvoiceDetails from "../screens/ClientScreens/Facturas/detailScreen";
import PayInvoices from "../screens/ClientScreens/Facturas/payInvoices";

import ClientReceipts from "../screens/ClientScreens/Recibos/screen";
import ReceiptDetails from "../screens/ClientScreens/Recibos/detailScreen";

import Tickets from "../screens/ClientScreens/Reclamos/screen";
import TicketDetails from "../screens/ClientScreens/Reclamos/detailScreen";
import CreateTicket from "../screens/ClientScreens/Reclamos/createScreen";

import CreditCards from "../screens/ClientScreens/Tarjetas/screen";
import CreditCardDetails from "../screens/ClientScreens/Tarjetas/detailScreen";
import AddCreditCard from "../screens/ClientScreens/Tarjetas/createScreen";

if (Platform.OS === 'android') {
  SafeAreaView.setStatusBarHeight(0);
}

export const AuthStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    Registro: {
      screen: RegistroScreen
    },
    Recuperar :{
      screen:RecuperarScreen
    },
  },
 { headerMode: 'screen' }
);

const UserStack = createStackNavigator({
  Users: {
    screen: usersScreen,

  },
  UserCreate: {
    screen: UsersCreateScreen,

  },
  UserDetail: {
    screen: UsersDetailScreen,

  },
}, {
  headerMode: "none",
});

const ProductStack = createStackNavigator({
  Products: {
    screen: ProductsScreen,

  },
  ProductCreate: {
    screen: ProductsCreateScreen,

  },
  ProductDetail: {
    screen: ProductsDetailScreen,

  },
}, {
  headerMode: "none",
});

const ReciboStack = createStackNavigator({
  Recibos: {
    screen: RecibosScreen,

  },
  ReciboCreate: {
    screen: RecibosCreateScreen,

  },
  ReciboDetail: {
    screen: RecibosDetailScreen,

  },
}, {
  headerMode: "none",
});

const ClientStack = createStackNavigator({
  Clients: {
    screen: ClientsScreen,

  },
  ClientCreate: {
    screen: ClientsCreateScreen,

  },
  ClientDetail: {
    screen: ClientsDetailScreen,

  },
}, {
  headerMode: "none",
});

const FacturaStack = createStackNavigator({
  Facturas: {
    screen: FacturasScreen,

  },
  FacturaCreate: {
    screen: FacturasCreateScreen,

  },
  FacturaDetail: {
    screen: FacturasDetailScreen,

  },
}, {
  headerMode: "none",
});

const ReclamoStack = createStackNavigator({
  Reclamos: {
    screen: ReclamosScreen,

  },
  ReclamoCreate: {
    screen: ReclamosCreateScreen,

  },
  ReclamoDetail: {
    screen: ReclamosDetailScreen,

  },
}, {
  headerMode: "none",
});

export const Root = createDrawerNavigator(
  {
    Users: {
      screen: UserStack,
    },
    Products: {
      screen: ProductStack,
    },
    Clients:{
      screen:ClientStack
    },
    Facturas:{
      screen: FacturaStack
    },
    Recibos:{
      screen: ReciboStack
    },
    Reclamos:{
      screen: ReclamoStack
    }
  } ,
  {
    contentComponent: SideBar,

  }
);

/***************** Clients app router *****************/

const FacturaClientesStack = createStackNavigator({
  Invoices: {
    screen: ClientInvoices,
  },
  InvoiceDetails: {
    screen: InvoiceDetails,
  },
  PayInvoices: {
    screen: PayInvoices,
  },
}, {
  headerMode: "none",
});

const ReciboClientesStack = createStackNavigator({
  Receipts: {
    screen: ClientReceipts,
  },
  ReceiptDetails: {
    screen: ReceiptDetails,
  },
}, {
  headerMode: "none",
});

const ReclamoClientesStack = createStackNavigator({
  Tickets: {
    screen: Tickets,

  },
  CreateTicket: {
    screen: CreateTicket,

  },
  TicketDetails: {
    screen: TicketDetails,

  },
}, {
  headerMode: "none",
});

const TarjetaClientesStack = createStackNavigator({
  CreditCards: {
    screen: CreditCards,

  },
  AddCreditCard: {
    screen: AddCreditCard,

  },
  CreditCardDetails: {
    screen: CreditCardDetails,

  },
}, {
  headerMode: "none",
});

export const ClientsRoot = createDrawerNavigator(
  {
   
    Facturas:{
      screen: FacturaClientesStack
    },
    /*Tarjetas: {
      screen: TarjetaClientesStack,
    },
    Recibos:{
      screen: ReciboClientesStack
    },*/
    Reclamos:{
      screen: ReclamoClientesStack
    }
  } ,
  {
    contentComponent: ClientsSidebar,

  }
);
